@isTest
private class Test_MOT_distributorSelectionV1 {

    static testMethod void distributorSelectionTest() {
        PageReference  pageRef = Page.MOT_distributorSelectionPage;
        Test.setCurrentPage(pageRef);

        //Only integration user since for heinz user price book is fixed to MOT-US so seeAll data need to be true.
        user usrIU = TestDataUtility.createIntUserData();
        system.assert(usrIU.id!=null);
        System.runAs(usrIU) {
        //Inserting ProfilePriceBookMapping__c custom setting
        TestDataUtility.creatPBCustomSettingData();
        //Accounts inserted
        List<Account> accLst = TestDataUtility.createAccountsData();
            accLst[1].Account_Type__c='Distributor';
            accLst[1].MOT_Distributor__c=true;
            accLst[1].MOT_Distributor_ID__c='test';
        update accLst;
        system.assert(accLst[1].id!=null);
        ID standardPB = Test.getStandardPricebookID();
        system.assert(standardPB!=null);
        //inserting Pricebook
        Pricebook2 newPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert newPB;
        system.assert(newPB.id!=null);
        //List of Product inserted
        List<Product2> prodLst = TestDataUtility.createMOTProductsData();
                system.assert(prodLst!=null);
        //List of PriceBookentry        
        List<PricebookEntry> pricebkEntryList = new List<PricebookEntry>();
        PricebookEntry pbEntry1 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[0]);
        pricebkEntryList.add(pbEntry1);
        PricebookEntry pbEntry2 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[1]);
        pricebkEntryList.add(pbEntry2);
        system.assert(pricebkEntryList[1].id!=null);
        // order orderObj inserted.
        List<order> orderList = TestDataUtility.createOrdersData(accLst);
        for(order o : orderList){
        o.pricebook2id=newPB.id;
        }
        update orderList;
        system.assert(orderList[0].id!=null);
        //OrderItem orItemObj inserted
        List<OrderItem> orItemList = TestDataUtility.createOrderItems(orderList,pricebkEntryList);
        system.assert(orItemList[1].id!=null);
        
        //Distributor product inserted
        List<Distributor_Products__c> dpList = new List<Distributor_Products__c>();
        Distributor_Products__c dp1 = new Distributor_Products__c(  Account__c=accLst[1].id,
                                                                    Product__c=prodLst[0].id);
            dpList.add(dp1);
        Distributor_Products__c dp2 = new Distributor_Products__c(  Account__c=accLst[1].id,
                                                                    Product__c=prodLst[1].id);
            dpList.add(dp2);
        Distributor_Products__c dp3 = new Distributor_Products__c(  Account__c=accLst[0].id,
                                                                    Product__c=prodLst[0].id);
            dpList.add(dp3);
        Distributor_Products__c dp4 = new Distributor_Products__c(  Account__c=accLst[0].id,
                                                                    Product__c=prodLst[1].id);
            dpList.add(dp4);
            insert dpList;
        system.assert(dpList[1].id!=null);
        
        ApexPages.currentPage().getParameters().put('id',orderList[0].id);
        ApexPages.currentPage().getParameters().put('acctId',accLst[0].id);
        ApexPages.currentPage().getParameters().put('distId',accLst[1].id);
        
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(orderList[0]);
        MOT_distributorSelection ds = new MOT_distributorSelection(con);
        Test.startTest();
            ds.orderid = orderList[0].id;
            ds.selectedDistributor = accLst[1];
            String savePage = ds.save().getUrl();
            System.assertEquals('/'+orderList[0].id, savePage); 
            String cancelPage = ds.cancel().getUrl();
            System.assertEquals('/'+orderList[0].id, cancelPage);
        Test.stopTest();
       }
    }
    //method where selected distributor is null
    static testMethod void distributorSelectionNullTest() {
        PageReference  pageRef = Page.MOT_distributorSelectionPage;
        Test.setCurrentPage(pageRef);

        //Only integration user since for heinz user price book is fixed to MOT-US so seeAll data need to be true.
        user usrIU = TestDataUtility.createIntUserData();
        system.assert(usrIU.id!=null);
        System.runAs(usrIU) {
        //Inserting ProfilePriceBookMapping__c custom setting
        TestDataUtility.creatPBCustomSettingData();
        //Accounts inserted
        List<Account> accLst = TestDataUtility.createAccountsData();
            accLst[1].Account_Type__c='Distributor';
            accLst[1].MOT_Distributor__c=true;
            accLst[1].MOT_Distributor_ID__c='test';
        update accLst;
        system.assert(accLst!=null);
        ID standardPB = Test.getStandardPricebookID();
        system.assert(standardPB!=null);
        //inserting Pricebook
        Pricebook2 newPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert newPB;
        system.assert(newPB.id!=null);
        //List of Product inserted
        List<Product2> prodLst = TestDataUtility.createMOTProductsData();
                system.assert(prodLst!=null);
        //List of PriceBookentry        
        List<PricebookEntry> pricebkEntryList = new List<PricebookEntry>();
        PricebookEntry pbEntry1 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[0]);
        pricebkEntryList.add(pbEntry1);
        PricebookEntry pbEntry2 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[1]);
        pricebkEntryList.add(pbEntry2);
        system.assert(pricebkEntryList[1].id!=null);
        // order orderObj inserted.
        List<order> orderList = TestDataUtility.createOrdersData(accLst);
        for(order o : orderList){
        o.pricebook2id=newPB.id;
        }
        update orderList;
        system.assert(orderList[0].id!=null);
        //OrderItem orItemObj inserted
        List<OrderItem> orItemList = TestDataUtility.createOrderItems(orderList,pricebkEntryList);
        system.assert(orItemList[1].id!=null);
        
        //Distributor product inserted
        List<Distributor_Products__c> dpList = new List<Distributor_Products__c>();
        Distributor_Products__c dp3 = new Distributor_Products__c(  Account__c=accLst[0].id,
                                                                    Product__c=prodLst[0].id);
            dpList.add(dp3);
        Distributor_Products__c dp4 = new Distributor_Products__c(  Account__c=accLst[0].id,
                                                                    Product__c=prodLst[1].id);
            dpList.add(dp4);
            insert dpList;
        system.assert(dpList[1].id!=null);
        
        ApexPages.currentPage().getParameters().put('id',orderList[0].id);
        ApexPages.currentPage().getParameters().put('acctId',accLst[0].id);
        ApexPages.currentPage().getParameters().put('distId',accLst[1].id);
        
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(orderList[0]);
        MOT_distributorSelection ds = new MOT_distributorSelection(con);
        Test.startTest();
            ds.selectedDistributor = null;
            // savepage url will be null
            PageReference savePage = ds.save();
            System.assertEquals(null,savePage);
        Test.stopTest();
        }
    }
    
    //method to show error messg 'CannotSubmitDistributor'
    static testMethod void ErrorMessgCantSubmitTest() {
        PageReference  pageRef = Page.MOT_distributorSelectionPage;
        Test.setCurrentPage(pageRef);

        //Only integration user since for heinz user price book is fixed to MOT-US so seeAll data need to be true.
        user usrIU = TestDataUtility.createIntUserData();
        system.assert(usrIU.id!=null);
        System.runAs(usrIU) {
        //Inserting ProfilePriceBookMapping__c custom setting
        TestDataUtility.creatPBCustomSettingData();
        //RecordType operatorRT =[select id from RecordType where name='' limit 1];
        //Accounts inserted
        List<Account> accLst = TestDataUtility.createAccountsData();
            accLst[1].Account_Type__c='Distributor';
            accLst[1].MOT_Distributor__c=true;
            accLst[1].MOT_Distributor_ID__c='test';
        update accLst;
        system.assert(accLst[1].id!=null);
        ID standardPB = Test.getStandardPricebookID();
        system.assert(standardPB!=null);
        //inserting Pricebook
        Pricebook2 newPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert newPB;
        system.assert(newPB.id!=null);
        //List of Product inserted
        List<Product2> prodLst = TestDataUtility.createMOTProductsData();
                system.assert(prodLst!=null);
        //List of PriceBookentry        
        List<PricebookEntry> pricebkEntryList = new List<PricebookEntry>();
        PricebookEntry pbEntry1 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[0]);
        pricebkEntryList.add(pbEntry1);
        PricebookEntry pbEntry2 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[1]);
        pricebkEntryList.add(pbEntry2);
        system.assert(pricebkEntryList[1].id!=null);
        // order orderObj inserted.
        List<order> orderList = TestDataUtility.createOrdersData(accLst);
        for(order o : orderList){
        o.pricebook2id=newPB.id;
        }
        update orderList;
        system.assert(orderList[0].id!=null);
        //OrderItem orItemObj inserted
        List<OrderItem> orItemList = TestDataUtility.createOrderItems(orderList,pricebkEntryList);
        system.assert(orItemList[1].id!=null);
        
        
        ApexPages.currentPage().getParameters().put('id',orderList[0].id);
        ApexPages.currentPage().getParameters().put('acctId',accLst[0].id);
        ApexPages.currentPage().getParameters().put('distId',accLst[1].id);
        
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(orderList[0]);
        MOT_distributorSelection ds = new MOT_distributorSelection(con);
        }
    }
    //method to get exception when distributor and present account are same.
    static testMethod void distrSeleCatchExpTest() {
        PageReference  pageRef = Page.MOT_distributorSelectionPage;
        Test.setCurrentPage(pageRef);

        //Only integration user since for heinz user price book is fixed to MOT-US so seeAll data need to be true.
        user usrIU = TestDataUtility.createIntUserData();
        system.assert(usrIU.id!=null);
        System.runAs(usrIU) {
        //Inserting ProfilePriceBookMapping__c custom setting
        TestDataUtility.creatPBCustomSettingData();
        //Accounts inserted
        List<Account> accLst = TestDataUtility.createAccountsData();
            accLst[1].Account_Type__c='Distributor';
            accLst[1].MOT_Distributor__c=true;
            accLst[1].MOT_Distributor_ID__c='test';
        update accLst;
        system.assert(accLst[1].id!=null);
        ID standardPB = Test.getStandardPricebookID();
        system.assert(standardPB!=null);
        //inserting Pricebook
        Pricebook2 newPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert newPB;
        system.assert(newPB.id!=null);
        //List of Product inserted
        List<Product2> prodLst = TestDataUtility.createMOTProductsData();
                system.assert(prodLst!=null);
        //List of PriceBookentry        
        List<PricebookEntry> pricebkEntryList = new List<PricebookEntry>();
        PricebookEntry pbEntry1 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[0]);
        pricebkEntryList.add(pbEntry1);
        PricebookEntry pbEntry2 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[1]);
        pricebkEntryList.add(pbEntry2);
        system.assert(pricebkEntryList[1].id!=null);
        // order orderObj inserted.
        List<order> orderList = TestDataUtility.createOrdersData(accLst);
        for(order o : orderList){
        o.pricebook2id=newPB.id;
        }
        update orderList;
        system.assert(orderList[0].id!=null);
        //OrderItem orItemObj inserted
        List<OrderItem> orItemList = TestDataUtility.createOrderItems(orderList,pricebkEntryList);
        system.assert(orItemList[1].id!=null);
        
        //Distributor product inserted
        List<Distributor_Products__c> dpList = new List<Distributor_Products__c>();
        Distributor_Products__c dp1 = new Distributor_Products__c(  Account__c=accLst[1].id,
                                                                    Product__c=prodLst[0].id);
            dpList.add(dp1);
        Distributor_Products__c dp2 = new Distributor_Products__c(  Account__c=accLst[1].id,
                                                                    Product__c=prodLst[1].id);
            dpList.add(dp2);
        Distributor_Products__c dp3 = new Distributor_Products__c(  Account__c=accLst[0].id,
                                                                    Product__c=prodLst[0].id);
            dpList.add(dp3);
        Distributor_Products__c dp4 = new Distributor_Products__c(  Account__c=accLst[0].id,
                                                                    Product__c=prodLst[1].id);
            dpList.add(dp4);
            insert dpList;
        system.assert(dpList[1].id!=null);
        
        ApexPages.currentPage().getParameters().put('id',orderList[0].id);
        ApexPages.currentPage().getParameters().put('acctId',accLst[0].id);
        ApexPages.currentPage().getParameters().put('distId',accLst[0].id);
        
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(orderList[0]);
        MOT_distributorSelection ds = new MOT_distributorSelection(con);
        Test.startTest();
            ds.orderid = orderList[0].id;
            ds.selectedDistributor = accLst[0];
            // savePage url will be null
            PageReference savePage = ds.save();
            System.assertEquals(null,savePage);
        Test.stopTest();
       }
    }
}