/***************************************************************************************************************************
*   Type        :   Test Data
*   Create By   :   Dipendu Chanda   - Deloitte USI
*   Create Date :   13.03.2015  
*   Description :   This class is used to create the test data for the test classes. Whenever a test class is required add data
                    in this class and use the appropriate methods to insert data in test classes.
*   Modification Log:
*   --------------------------------------------------------------------------------------
*   * Developer                   Date          Description
*   * ------------------------------------------------------------------------------------                 
*   * Dipendu Chanda                28.05.2015      Created functions: creatPBCustomSettingData,createUserData,createCampaignData,createAccountData,createMOTProductData
*   * Dipendu Chanda                05.06.2015      Added functions: createMOTProductData(),createAccountData()
*   * Dipendu Chanda                12.06.2015      Added Functions: createIntUserData(),createPricebookEntry(),createProductData()
*****************************************************************************************************************************/
@isTest
public with sharing class TestDataUtility {
    //  To insert PricebookProfileMapping__c custom setting
    public static void creatPBCustomSettingData(){
            List<ProfilePriceBookMapping__c> prBookPfrMapCS = new List<ProfilePriceBookMapping__c>();
            ProfilePriceBookMapping__c cs=new ProfilePriceBookMapping__c(); //Custom Setting pricebook
            cs.Name='US_MOT_Profile_Mapping';//Static record 
            prBookPfrMapCS.add(cs);
            Database.insert(prBookPfrMapCS);
            system.assert(prBookPfrMapCS!=null);
        }
      // To insert an user with Heinz Sales User profile 
      public static user createUserData(){
        Profile p = [select name, id from profile where name= 'Heinz Sales User']; 
        system.assert(p.id!=null);
        String testUserName = String.valueOf(System.now().getTime()) + '@hTest.com';
        User usrHNZ = new User( alias = 'TestUser', email='Newtest@email.com',
                                emailencodingkey='UTF-8', lastname='TestUser1', languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Denver', username=testUserName, isActive=true);
        Database.insert(usrHNZ);
        system.assert(usrHNZ.id!=null);
        return usrHNZ;
    }
    // To insert an user with Integration User profile 
      public static user createIntUserData(){
        Profile p = [select name, id from profile where name= 'Integration User'];
        system.assert(p.id!=null); 
        String testUserName = String.valueOf(System.now().getTime()) + '@hTest.com';
        User usrIU= new User( alias = 'TestIU', email='Newtest@email.com',
                                emailencodingkey='UTF-8', lastname='TestUser1', languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Denver', username=testUserName, isActive=true);
        Database.insert(usrIU);
        system.assert(usrIU.id!=null);
        return usrIU;
    }
      // to insert an active campaign
      public static Campaign createCampaignData(){    
        Campaign campaignObj = new Campaign(Name = 'Test Campaign',
                                            IsActive = true);
        Database.insert(campaignObj);
        system.assert(campaignObj.id!=null);
        return campaignObj;
    }
    // to insert an account
    public static Account createAccountData(){ 
        String testName = String.valueOf(System.now().getTime()) + 'Test';   
        Account acc= new Account(   Name=testName+ 'Account',
                                    ShippingCity = 'Test',
                                    ShippingCountry = 'Test',
                                    ShippingPostalCode = '546666',
                                    ShippingState = 'Test',
                                    ShippingStreet = 'Test'); 
                                    
        Database.insert(acc);
        system.assert(acc.id!=null);
        return acc;
    }
    // to insert a list of account
    public static List<Account> createAccountsData(){ 
        String testName = String.valueOf(System.now().getTime()) + 'Test'; 
        List<Account> accList = new List<Account>();
        Account acc1= new Account(   Name=testName+ 'Accnt1',
                                    ShippingCity = 'Test',
                                    ShippingCountry = 'Test',
                                    ShippingPostalCode = '546666',
                                    ShippingState = 'Test',
                                    ShippingStreet = 'Test'); 
        accList.add(acc1); 
        Account acc2= new Account(   Name='Accnt2 '+testName,
                                    ShippingCity = 'Test2',
                                    ShippingCountry = 'Test2',
                                    ShippingPostalCode = '5466662',
                                    ShippingState = 'Test2',
                                    ShippingStreet = 'Test2'); 
        accList.add(acc2); 
        Database.insert(accList);
        system.assert(accList!=null);
        return accList;
    }
    //to insert MOT product
    public static Product2 createMOTProductData(){
        String testName = String.valueOf(System.now().getTime()) + 'Test';
        Product2 prodObj = new Product2(Name= testName+ 'Product',
                                        ProductCode='TestCode',
                                        MOT_Order_Product_Limit__c=10,
                                        MOT_Product__c=true,
                                        Demand_Planner_Email__c='abctest@abc.com',
                                        Product_Group_Level_5__c='test',
                                        IsActive=true);
        Database.insert(prodObj);
        system.assert(prodObj.id!=null);
        return prodObj;
    }
    //to insert a list of MOT product
    public static List<Product2> createMOTProductsData(){
        String testName = String.valueOf(System.now().getTime()) + 'Test';
        List<Product2> prodList = new List<Product2>();
        Product2 prodObj1 = new Product2(Name= testName+ 'Prod1',
                                        ProductCode='TestCode1',
                                        MOT_Order_Product_Limit__c=10,
                                        MOT_Product__c=true,
                                        Demand_Planner_Email__c='abctest@abc.com',
                                        Product_Group_Level_5__c='test',
                                        IsActive=true);
        prodList.add(prodObj1);
        Product2 prodObj2 = new Product2(Name= testName+ 'Prod2',
                                        ProductCode='TestCode2',
                                        MOT_Order_Product_Limit__c=10,
                                        MOT_Product__c=true,
                                        Demand_Planner_Email__c='abctest@abc.com',
                                        Product_Group_Level_5__c='test',
                                        IsActive=true);
        prodList.add(prodObj2);
        Database.insert(prodList);
        system.assert(prodList!=null);
        return prodList;
    }
    //to insert Simple product
    public static Product2 createProductData(){
        String testName = String.valueOf(System.now().getTime()) + 'Test';
        Product2 prodObj = new Product2(Name= testName+ 'Product',
                                        ProductCode='TestCode',
                                        Demand_Planner_Email__c='abctest@abc.com',
                                        Product_Group_Level_5__c='test',
                                        IsActive=true);
        Database.insert(prodObj);
        system.assert(prodObj.id!=null);
        return prodObj;
    }
    //to insert a list of Simple product
    public static List<Product2> createProductsData(){
        String testName = String.valueOf(System.now().getTime()) + 'Test';
        List<Product2> prodList = new List<Product2>();
        Product2 prodObj1 = new Product2(Name= testName+ 'Prod1',
                                        ProductCode='TestCode1',
                                        Demand_Planner_Email__c='abctest@abc.com',
                                        Product_Group_Level_5__c='test',
                                        IsActive=true);
        prodList.add(prodObj1);
        Product2 prodObj2 = new Product2(Name= testName+ 'Prod2',
                                        ProductCode='TestCode2',
                                        Demand_Planner_Email__c='abctest@abc.com',
                                        Product_Group_Level_5__c='test',
                                        IsActive=true);
        prodList.add(prodObj2);
        Database.insert(prodList);
        system.assert(prodList!=null);
        return prodList;
    }
    //to insert a list of Order
    public static List<Order> createOrdersData(List<Account> accList){
        String testName = String.valueOf(System.now().getTime()) + 'Test';
        List<Order> ordList = new List<Order>();
        order orderObj1 = new order( EffectiveDate = Date.today().addDays(-10), 
                                    Requested_Delivery_Date__c = Date.today().addDays(+10),
                                    Status = 'Draft',
                                    AccountId = accList[0].id,
                                    ShippingCity = 'Test',
                                    ShippingCountry = 'Test',
                                    ShippingPostalCode = '546666',
                                    ShippingState = 'Test',
                                    ShippingStreet = 'Test');
        ordList.add(orderObj1);
        order orderObj2 = new order( EffectiveDate = Date.today().addDays(-9), 
                                    Requested_Delivery_Date__c = Date.today().addDays(+9),
                                    Status = 'Draft',
                                    AccountId = accList[0].id,
                                    ShippingCity = 'Test2',
                                    ShippingCountry = 'Test2',
                                    ShippingPostalCode = '5466662',
                                    ShippingState = 'Test2',
                                    ShippingStreet = 'Test2');
        ordList.add(orderObj2);
        Database.insert(ordList);
        system.assert(ordList!=null);
        return ordList;
    }
    //to insert pricebookentry
    public static PricebookEntry createPricebookEntry(ID standard, Pricebook2 newPricebook, Product2 prod) {
        PricebookEntry one = new PricebookEntry();
            one.pricebook2Id = standard;
            one.product2id = prod.id;
            one.unitprice = 1249.0;
            one.isactive = true;
        insert one;
        PricebookEntry ret = new PricebookEntry();
            ret.pricebook2Id = newPricebook.id;
            ret.product2id = prod.id;
            ret.unitprice = 1250.0;
            ret.isactive = true;
        insert ret;
        system.assert(ret.id!=null);
        return ret;
    } 
    // to insert OrderItem
    public static List<OrderItem> createOrderItems( List<Order> OrdList,List<PricebookEntry> pbEntryList){
        List<OrderItem> ordItemList = new List<OrderItem>();
        OrderItem orItemObj1 = new OrderItem(OrderId = OrdList[0].id,
                                            PricebookEntryId = pbEntryList[0].id,
                                            Quantity = 2,
                                            UnitPrice=100, 
                                            ServiceDate=Date.Today());
        ordItemList.add(orItemObj1);
        OrderItem orItemObj2 = new OrderItem(OrderId = OrdList[1].id,
                                            PricebookEntryId = pbEntryList[1].id,
                                            Quantity = 2,
                                            UnitPrice=100, 
                                            ServiceDate=Date.Today()+1);
        ordItemList.add(orItemObj2);
        insert ordItemList;
        system.assert(ordItemList[1].id!=null);
        return ordItemList;
    }    
}