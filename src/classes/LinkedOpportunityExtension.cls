/*********************************************************************************
 Author       :   Dipika (Appirio Offshore)
 Created Date :   June 18,2013
 Task         :   T-152839
 Description  :   Create and Clone Opportunities
*********************************************************************************/
public class LinkedOpportunityExtension {
	public Opportunity opp{get;set;}
	private list<Opportunity> listOpp;
	private String requestedOppName;
	private Id AccountId;
	
	public LinkedOpportunityExtension(ApexPages.standardController con){
		opp = new Opportunity();
		listOpp = new list<Opportunity>();
		if ( ApexPages.currentPage().getParameters().get('AccountId') != null){
			list<Account> listAcc = new list<Account>([Select Id From Account Where Id =: ApexPages.currentPage().getParameters().get('AccountId')]);
			if(listAcc.size()>0){
				AccountId = listAcc.get(0).Id;
				opp.AccountId = AccountId;
			}
			
		}
	}
	
	public pageReference saveOpportunities(){
		try{
			requestedOppName  = opp.Name;
			opp.Name = requestedOppName + ' - Step 1';
			opp.StageName ='New Opportunity';
			insert opp;
			if(opp.of_Linked_Opportunities__c!=null){
				for(Integer counter =0; counter< Integer.valueOf(opp.of_Linked_Opportunities__c) - 1 ; counter++){
					Opportunity opportunity = opp.clone(false, true);
					opportunity.Name = requestedOppName +' - Step '+ String.valueOf(counter+2);
					opportunity.Primary_Opportunity__c = opp.Id;
					opportunity.of_Linked_Opportunities__c = null;
					listOpp.add(opportunity);	
				}
			}
			
			if(listOpp.size()>0){
				insert listOpp;
			}
			if(AccountId != null){
				return new pageReference('/'+AccountId);
			}
			else{
				return new pageReference('/'+opp.Id);
			}
		}
		catch(Exception ex){
			ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,ex.getMessage()));
			return null;
		}
	}
}