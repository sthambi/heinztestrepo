/*
        * Developed by : Ankita
        *
        * Functionality : Automatically nulls the uniqueproduct id field when order is cloned with products
        *
        Date:       Developer:      Modification Description:
        06/01/2015    (Ankita)        created.
                                     
        */
    public with sharing class MOT_OrderProductTriggerHandler{
        //Automatically nulls the uniqueproduct id field when order is cloned with products
        public static void uniqueProductNull(List<OrderItem> lstNewOrderItem){
        for(OrderItem oItem: lstNewOrderItem){
            oItem.Unique_Product_ID__c=null;     
        }
        }
        
        //Automatically Checks duplicate product existed already or not
        public static void UniqueProduct(List<OrderItem> lstNewOrderItem){
        List<ID> obj2IDs = new List<ID>();        
        for (OrderItem obj: lstNewOrderItem){
            //Creating a list of order whose orderItem is getting inserted
            obj2IDs.add(obj.Orderid);
        }
        //querying the reduced list of all orderitem present in the orders(order which has been modified).
        List <OrderItem> ordItmLst = [ SELECT Id, Unique_Product_ID__c 
                                        FROM OrderItem 
                                        Where Orderid in: obj2IDs];
        //for all OrderItems we are getting from trigger
        for (OrderItem obj: lstNewOrderItem){
              //since newly created we are populating the field value 'Unique_Product_ID__c'
              obj.Unique_Product_ID__c = obj.OrderId+'_'+obj.PricebookentryId;
              if(obj.Unique_Product_ID__c!=null){
                  //for all the orderItem in the order edited
                  for(OrderItem allOI: ordItmLst){
                    //if the Unique_Product_ID__c is already existing show an error on the quantity field.
                    if(obj.Unique_Product_ID__c == allOI.Unique_Product_ID__c){
                        obj.Quantity.addError('You have already added this product to this order. Return to the order to update the existing product quantity.');
                        }
                    }
                }                  
           }  
    }
    }