/**
 *    @author Gurumurthy Raghuraman
 *    @date   10/06/2015
      @description: This class includes all the functions which are called from MUDA_InventoryTrigger.
      Modification Log: NA
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Gurumurthy Raghuraman           10/06/2015          Original Version
**/

public with sharing class MUDA_InventoryTriggerHandler{

         public static void MUDA_Assign_Planner(List<Material_Plant_Batch__c> lstInventory){
               Set<String> setPlantMaterialCode=new Set<String>();    // Set of all plant material code combination
               Map<String,String> mapPlntMatUser=new Map<String,String>(); // Map of Plant_Material_Code__c and Planner
               String strPlantMaterialCode;
               for(Material_Plant_Batch__c objInventory:lstInventory){
                    strPlantMaterialCode=objInventory.Plant_Code__c+objInventory.Material_Number__c;
                    setPlantMaterialCode.add(strPlantMaterialCode);   
               }
               try{
                  for(Inventory_Owner__c objInventoryOwner:[SELECT id,Plant_Material_Code__c,Planner_User__c from Inventory_Owner__c WHERE Plant_Material_Code__c IN:setPlantMaterialCode]){
                     if(objInventoryOwner.Planner_User__c!=null){
                        mapPlntMatUser.put(objInventoryOwner.Plant_Material_Code__c,objInventoryOwner.Planner_User__c);
                     }
                  }
               }
               catch(DMLException e){
                    System.debug('Error Message' + e.getMessage());
               }
               
               for(Material_Plant_Batch__c objInventory:lstInventory){
                     strPlantMaterialCode=objInventory.Plant_Code__c+objInventory.Material_Number__c;
                     if(mapPlntMatUser.containsKey(strPlantMaterialCode)){
                          objInventory.Inventory_Owner__c=mapPlntMatUser.get(strPlantMaterialCode);
                     }
                     
               }
                                 
             
         
         }
         
}