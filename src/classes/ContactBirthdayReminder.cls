/**************************************************************************/
// Class: ContactBirthdayReminder
//
// Author:  Herb Whitacre - Appirio
// Date:  5/21/2013
//
// Description:
//	This class should be scheduled to run nightly.
//	The class works in conjunction with Contact "Birthday" fields
//	and Workflow Rules, to send email alerts to Contact Owners
//	when the Contact's birthday is approaching.
//	
// Pattern of execution:
//	1) Formula fields on the Contact compute each Contact's
//		next birthday and reminder date, in real-time.
//	<begin repeating cycle>
//	2) Scheduled Apex executes this class once per day.
//	3) This class finds contacts whose reminder date is today
//		and sets the flag Birthday_Reminder_Send_Now__c = True.
//	4) Workflow Rule fires upon the flag being set True. The workflow
//		immediately performs the following:
//		a) Sends Email Alert to the Contact Owner. An email template is
//			used with merge fields about the contact and her birthday.
//		b) Resets the Birthday_Reminder_Send_Now__c flag back to False.
//		c) Sets the Birthday_Reminder_Last_Sent__c field to
//			the current date/time stamp.
//	<end repeating cycle>
//	5) As time goes on, the "Next Birthday" formula fields continually
//		update. After this year's birthday is past, those fields
//		will reflect next year's birthday, and the cycle will
//		repeat next year when the time comes.
//
// -----------   Comments, Notes, and Instructions --------------------------
//
//	TO CHANGE # OF DAYS IN ADVANCE THAT REMINDERS ARE SENT:
//		Edit the formula field "Contact.Next_Birthday_Reminder_Date__c".
//		At design time, the formula is:  Next_Birthday__c - 7
//		which means reminders fire 7 days before next birthday.
//		Change the "7" to another value, in days.
//		
//	TO CUSTOMIZE THE EMAIL NOTIFICATION:
//		Edit the Email Template "Contact Birthday Reminder".
//
//	TO DISABLE NOTIFICATIONS:
//		Disable or Delete the Scheduled Apex Job that runs this class
//		so that it does not run.
//			This is the best method because it won't adversely affect 
//			anything else, and functionality can be easily resumed
//			later by editing or re-creating the job schedule.
//	
//
/***************************************************************************/

global class ContactBirthdayReminder implements Schedulable{
	
	//mandatory function called by the Apex Scheduler
    global void execute(SchedulableContext SC) {
        setReminderFlag(); // our main function that flags the contacts
    }//end execute()
    
    public void setReminderFlag() {
    	
    	//find contacts whose Birthday Reminder needs sent today
        Contact[] c = [SELECT Id, Name FROM Contact
                       WHERE Birthdate != null
                       AND Next_Birthday_Reminder_Date__c = TODAY
                       ];
		
		if(c.size() == 0) {
			System.Debug('\n********No birthdays found for today');
		}
		else {
			//flag the contact for Reminder to be sent
			//   (This simply will trigger the Workflow Rule.)
			//   (The workflow will do the email alert, mark a timestamp, and reset the flag.)
			for(Contact con: c) {
				System.Debug('\n********Birthday for: ' + con.Name);
	        	con.Birthday_Reminder_Send_Now__c = true;
	        }
	        try {
	        	System.Debug('\n********Updating Contacts');
	        	update c;
	        }
	        catch (Exception e) {
	        	System.Debug('\n********Exception(s) occurred while updating contacts');
	        	System.Debug(e);
	        } //end try-catch
		} //end if
		
    } //end setReminderFlag()
    
} //end class ContactBirthdayReminder