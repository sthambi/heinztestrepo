/***************************************************************************************************************************
*   Type        :   Test Class
*   Name        :   Test_MOT_OrderProductTriggerHandler
*   Create By   :   Dipendu Chanda   - Deloitte USI
*   Description :   This class is the test class for MOT_OrderProductTriggerHandler and OrderProductTrigger
*   Modification Log:
*   --------------------------------------------------------------------------------------
*   * Developer                   Date          Description
*   * ------------------------------------------------------------------------------------                 
*   * Dipendu Chanda                5.06.2015      Created 
*****************************************************************************************************************************/

@isTest
private class Test_MOT_OrderProductTriggerHandler{
    static testMethod void OrderPrUniquePrdId() {
        //Only integration user
        user usrIU = TestDataUtility.createIntUserData();
        system.assert(usrIU.id!=null);
        System.runAs(usrIU) {
        //Inserting ProfilePriceBookMapping__c custom setting
        TestDataUtility.creatPBCustomSettingData();
        //Accounts inserted
        List<Account> accLst = TestDataUtility.createAccountsData();
        system.assert(accLst!=null);
        //fetching Standard priceBook
        ID standardPB = Test.getStandardPricebookID();
        system.assert(standardPB!=null);
        //inserting Pricebook
        Pricebook2 newPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert newPB;
        system.assert(newPB.id!=null);
        //List of Product inserted
        List<Product2> prodLst = TestDataUtility.createMOTProductsData();
                system.assert(prodLst!=null);
        //List of PriceBookentry        
        List<PricebookEntry> pricebkEntryList = new List<PricebookEntry>();
        PricebookEntry pbEntry1 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[0]);
        pricebkEntryList.add(pbEntry1);
        PricebookEntry pbEntry2 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[1]);
        pricebkEntryList.add(pbEntry2);
        system.assert(pricebkEntryList[1].id!=null);
        
        // order orderObj inserted.
        List<order> orderList = TestDataUtility.createOrdersData(accLst);
        for(order o : orderList){
        o.pricebook2id=newPB.id;
        }
        update orderList;
        //orderList[1].pricebook2id=newPB.id;
        system.assert(orderList[0].id!=null);
        //OrderItem orItemObj inserted
        List<OrderItem> orItemList = TestDataUtility.createOrderItems(orderList,pricebkEntryList);
        system.assert(orItemList[1].id!=null);
        //Duplicate OrderItem orItemObj inserted
        try{ 
        List<OrderItem> orItemList2 = TestDataUtility.createOrderItems(orderList,pricebkEntryList);
         }
        catch(DMLException ex){
            //Showing the exception that occured ie."DUPLICATES DETECTED"
            System.Debug('There was an error ' + ex.getMessage());
            }        
       }
    }
}