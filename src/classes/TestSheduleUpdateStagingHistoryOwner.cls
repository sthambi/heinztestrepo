/**
* Class Name   : TestSheduleUpdateStagingHistoryOwner
* Created By   : Dipika Gupta(Appirio Offshore)
* Created Date : 11th June 2013
* Purpose      : T-149805 testClass for batch class UpdateStagingHistoryOwner
**/
@istest
private class TestSheduleUpdateStagingHistoryOwner {
	private static Stage_History__c SHObj;
	private static User usr;
	
	//Test method for SheduleUpdateStagingHistoryOwner class
	static testmethod void testShedule() {
		createTestData();
		String CRON_EXP = '0 0 0 * * ?';
		Test.startTest();
		SheduleUpdateStagingHistoryOwner SUSUHO = new SheduleUpdateStagingHistoryOwner();
		String jobId = System.schedule('testScheduledApex', CRON_EXP, SUSUHO);
		
		// Get the information from the CronTrigger API object  
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                            FROM CronTrigger 
                            WHERE id = :jobId];
		// Verify the expressions are the same  
        System.assertEquals(CRON_EXP, ct.CronExpression);
    
        // Verify the job has not run  
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
	
	//test method for UpdateStagingHistoryOwner batch class
    static testmethod void testBatch() {
		Test.startTest();
		createTestData();
		UpdateStagingHistoryOwner STOObj =  new UpdateStagingHistoryOwner();
		database.executeBatch(STOObj);
        Test.stopTest();
        SHObj = [SELECT ID, OwnerId, Owner_ID__c,CreatedDate FROM Stage_History__c Where ID =: SHObj.Id];
        System.assertEquals(SHObj.OwnerID, usr.Id);
    }
	
	//Create test data
	private static void createTestData(){
		Account acc = new Account(Name = 'Test Account');
        insert acc;
		createUser('UserTestdipika@gmail.com','System Administrator','test@gmail.com');
		SHObj= new Stage_History__c(Account__c = acc.id,
								    Stage__c = 'New Opportunity',
									owner_id__c = usr.Id);
		insert SHObj;											 
	}
	
	//Create test user
	private static Void createUser(String UserName, String strProfileName, string strEmail) {
        Profile testProfile = [select id
        from profile where name = :strProfileName LIMIT 1];
          
        usr = new User(alias = 'xyz-010', email = strEmail , 
        emailencodingkey = 'UTF-8', 
        lastname = 'TestLastName', 
        languagelocalekey = 'en_US', 
        localesidkey = 'en_US', 
        profileid = testProfile.id, 
        timezonesidkey = 'America/Los_Angeles', 
        username = UserName);
        insert usr;
  }

}