/***************************************************************************************************************************
*   Type        :   Test Class
*   Name        :   Test_MOT_UpdatePriceBookHandler
*   Create By   :   Dipendu Chanda   - Deloitte USI
*   Description :   This class is the test class for MOT_OrderTriggerHandler.updatePriceBook and OrderTrigger
*   Modification Log:
*   --------------------------------------------------------------------------------------
*   * Developer                   Date          Description
*   * ------------------------------------------------------------------------------------                 
*   * Dipendu Chanda                28.05.2015      Created 
*****************************************************************************************************************************/
@isTest
private class Test_MOT_UpdatePriceBookHandler {
    static testMethod void priceBookEntryCheck() {
        //Only Heinz sales user
        user usrHNZ = TestDataUtility.createUserData();
        system.assert(usrHNZ.id!=null);
        System.runAs(usrHNZ ) {
        //Inserting ProfilePriceBookMapping__c custom setting
        TestDataUtility.creatPBCustomSettingData();
        //Accounts inserted
        List<Account> accLst = TestDataUtility.createAccountsData();
        system.assert(accLst!=null);
        //Inserting order 
        List<order> orderList = TestDataUtility.createOrdersData(accLst);
        system.assert(orderList!=null);
       }
    }
}