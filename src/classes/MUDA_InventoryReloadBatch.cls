/**
 *    @author Nikhil More
 *    @date   26/05/2015
      @description    Batch Job for inventory assignment.
      Modification Log: 
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Nikhil More                  26/05/2015            Original Version
 */
global class MUDA_InventoryReloadBatch implements Database.Batchable<SObject>, Database.Stateful
{
    
    global MUDA_InventoryReloadBatch()
    {        
        
    }
    
    global Database.queryLocator start(Database.BatchableContext ctx)
    {
        if(!Test.isRunningTest())
        {
             // return Database.getQueryLocator([SELECT Demand_Assigned__c,Id FROM Material_Plant_Batch__c WHERE Demand_Assigned__c = true]);
             return Database.getQueryLocator([SELECT Demand_Assigned__c,Id FROM Material_Plant_Batch__c]);
        }
        else
        {
             return Database.getQueryLocator([SELECT Demand_Assigned__c,Id FROM Material_Plant_Batch__c WHERE Demand_Assigned__c = true LIMIT 1]);
        }
    }
    
    global void execute(Database.BatchableContext ctx, List<Sobject> scope)
    {        
        List<Material_Plant_Batch__c> dfList = (List<Material_Plant_Batch__c>)scope;    
        
        for(Material_Plant_Batch__c mpb : dfList)
        {
            mpb.Demand_Assigned__c = FALSE;
            mpb.At_Risk__c = FALSE;
            mpb.Daily_Demand__c = null;
            mpb.Cumulative_Consumption__c = 0;
            mpb.Daily_Consumption__c = 0;
        }
            
        if(dfList.size()>0)
        {
            Database.update(dfList);
        }
    }    
    
    global void finish(Database.BatchableContext ctx)
    {
    
    }
}