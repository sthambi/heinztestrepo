/*********************************************************************************
 Author       :   Dipika (Appirio Offshore)
 Created Date :   June 10,2013
 Task         :   T-149806
 Description  :   Redirect to external URL on Opportunity Save
*********************************************************************************/
public with sharing class redirectToExtUrlExtension {
    public Opportunity opp {get;set;}
    public boolean isredirect{get;set;}
    public redirectToExtUrlExtension (ApexPages.standardController con){
        opp = (Opportunity)con.getRecord();
        opp = [select hasClosed__c from Opportunity where id =: opp.Id ];
        isredirect = false;
    }
    public pageReference redirect(){
        if(opp.hasClosed__c == true){
            opp.hasClosed__c = false;
            update opp;
            isredirect = true;
        }
        return null;
    }
}