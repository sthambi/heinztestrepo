/**
* Class Name   : TestChatterAutoFollowController Class
* Created By   : Kirti Agarwal
* Created Date : 24th April 2013
* Purpose      : test class for ChatterAutoFollowController
**/
@isTest
public with sharing class TestChatterAutoFollowController {
 public static testmethod void testRunNow(){
        test.startTest();
        try{
            ChatterAutoFollowController objChatterAutoFollowController = new ChatterAutoFollowController();
            PageReference page = objChatterAutoFollowController.Nightly();
        }
        catch(Exception e){             
        }
        ChatterAutoFollowController c = new ChatterAutoFollowController();
        PageReference pageResult = c.RunNow();
        system.assert(pageResult == null);  
        test.stopTest();
        
    }
}