/**
 *    @author Nikhil More
 *    @date   26/05/2015
      @description    US-0104, US-0107: Track Field History for Action Plans. If the strategy or status is changed, capture this for history report.
      Modification Log: 
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Nikhil More                  26/05/2015            Original Version
 */

Public Class MUDA_PlantTriggerHelper
{

   /*
   *  @author  Nikhil More 
   *  @description : Update The Demand Plant Code on Plants
   *  @param List<Plant__c> newPlantList
   *  @return  None.
   */

    public static void handleBeforeInsertMethod(List<Plant__c> newPlantList)
    {
        Set<Id> demandPlantIds = new Set<Id>();
        List<Plant__c> demandPlantList = new List<Plant__c>();
        Map<Id, Plant__c> mapIdvsDemandPlant = new Map<Id, Plant__c>();
        
         for(Plant__c p : newPlantList)
         {
             p.Demand_Plant_Code__c = p.Plant_Code__c;
         }
    }
    
    /*
   *  @author  Nikhil More 
   *  @description : Update The Demand Plant Code from Demand Plant look up value
   *  @param List<Plant__c> newPlantList
   *  @return  None.
   */

    public static void handleBeforeUpdateMethod(List<Plant__c> newPlantList)
    {
        Set<Id> demandPlantIds = new Set<Id>();
        List<Plant__c> demandPlantList = new List<Plant__c>();
        Map<Id, Plant__c> mapIdvsDemandPlant = new Map<Id, Plant__c>();
        
        for(Plant__c p : newPlantList)
        {
            if(p.Demand_Plant__c != null)
            {
                demandPlantIds.add(p.Demand_Plant__c);
            }
            else
            {
                p.Demand_Plant_Code__c = p.Plant_Code__c;
            }
        }
        if(!demandPlantIds.isEmpty())
        {
            demandPlantList = [SELECT Demand_Plant_Code__c,Demand_Plant__c,Id,Name,OwnerId,Plant_Code__c,Postal_Code__c 
                                      FROM Plant__c
                                      WHERE Id IN : demandPlantIds];
            
            for(Plant__c dp : demandPlantList)
            {
                mapIdvsDemandPlant.put(dp.Id, dp);
            }
        }

        for(Plant__c mp : newPlantList)
        {
            if(mp.Demand_Plant__c != null)
            {
                mp.Demand_Plant_Code__c = mapIdvsDemandPlant.get(mp.Demand_Plant__c).Plant_Code__c;
            }
            else
            {
                mp.Demand_Plant_Code__c = mp.Plant_Code__c;
            }
        }
    }
}