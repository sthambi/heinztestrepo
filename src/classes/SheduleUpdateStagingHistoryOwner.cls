/**
* Class Name   : SheduleUpdateStagingHistoryOwner
* Created By   : Dipika Gupta(Appirio Offshore)
* Created Date : 11th June 2013
* Purpose      : T-149805 shedulable class to update Stage History OwnerId
**/
global class SheduleUpdateStagingHistoryOwner implements Schedulable{
	global void execute(SchedulableContext SC) {
		UpdateStagingHistoryOwner USHO = new UpdateStagingHistoryOwner();
		ID batchprocessid = Database.executeBatch(USHO);
	}
}