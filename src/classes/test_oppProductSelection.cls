@isTest(OnInstall=true)
private class test_oppProductSelection {

    static testMethod void oppProductSelectionTest() {
        PageReference  pageRef = Page.oppOverride3;
       
        Product2 prodObj = new Product2(Name='TestProduct',ProductCode='TestCode');
        insert prodObj;
        
        PriceBook2 priceBkObj = [select Id from Pricebook2 where isStandard=true limit 1];
        PricebookEntry priceBookEntryObj = new PricebookEntry (Product2Id=prodObj.Id,Pricebook2Id=priceBkObj.Id,UnitPrice=100,isActive=true);
        insert priceBookEntryObj;
        Opportunity oppObj = new Opportunity(Name='Test',StageName='Prospecting',CloseDate=System.Today().addDays(1),Pricebook2Id=priceBkObj.Id);
        insert oppObj;
        
        test.startTest();
        ApexPages.StandardController mc = new ApexPages.StandardController(oppObj);
        oppProductSelectionCon3 controller = new oppProductSelectionCon3(mc);
        //testing the autosuggest method
        list<PriceBookEntry> result = oppProductSelectionCon3.returnMatchingRec('TestProduct','Name',priceBkObj.Id);
        System.assertEquals('TestProduct',result[0].Name);
        //Selecting a product
        controller.prodId = priceBookEntryObj.Id;
        controller.selectProduct();
        //checking if the selected product is added
        System.assertEquals(controller.wrapProductList[0].priceBookProd.Id, priceBookEntryObj.Id);
        //Looping inside already selected
        controller.selectProduct();
        //removing products
        controller.prodIndex = 0;
        controller.removeProduct();
        //Checking that no product is selected
        System.assertEquals(0,controller.wrapProductList.size());
        //Again selecting product
        controller.prodId = prodObj.Id;
        controller.selectProduct();
        controller.saveProducts();
        //selecting product to save
        controller.prodId = priceBookEntryObj.Id;
        controller.selectProduct();
        //save product w/o mandatory fields
         controller.saveProducts();
         //Saving
         controller.wrapProductList[0].oppLineItem.quantity = 1;
         controller.saveProducts();
        
        test.stopTest();
    }
}