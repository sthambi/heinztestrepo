/**
 *    @author Gurumurthy Raghuraman
 *    @date   17/06/2015
      @description: Test Class for Test_MUDAInventoryTriggerHandler.
      Modification Log: NA
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Gurumurthy Raghuraman           17/06/2015          Original Version
**/

@isTest
private class Test_MUDAInventoryTriggerHandler{

     static testMethod void unitTest1(){
              List<Profile> lstProfile=[SELECT Id,Name FROM Profile WHERE Name='System Administrator' OR Name='MUDA Planner'];
              List<User> lstUser=new List<User>();
              User objTestUser1=new User(Alias = 'system', Email='systemadmin@testorg.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = lstProfile[1].Id, 
                  TimeZoneSidKey='America/Los_Angeles', UserName='systemadmin@testorg.com');
              lstUser.add(objTestUser1);
              
              User objTestUser2=new User(Alias = 'MUDAPl', Email='MUDAPlanner@testorg.com', 
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                  LocaleSidKey='en_US', ProfileId = lstProfile[0].Id, 
                  TimeZoneSidKey='America/Los_Angeles', UserName='MUDAPlanner@testorg.com');
              lstUser.add(objTestUser2);
              
              insert lstUser;
              
              test.StartTest();
              System.runAs(objTestUser1){
                   Plant__c objTestPlant1=new Plant__c();
                   objTestPlant1.Name='Test Plant1';
                   objTestPlant1.Plant_Code__c='1234';
                   insert objTestPlant1;
                   
                   Material__c objTestMaterial1=new Material__c();
                   objTestMaterial1.Name='Test Material1';
                   objTestMaterial1.Material_Number__c='6547';
                   insert objTestMaterial1;
               
                   Inventory_Owner__c objInvOwner1=new  Inventory_Owner__c();
                   objInvOwner1.Plant_Code__c='1234';
                   objInvOwner1.Material_Code__c='6547';
                   objInvOwner1.Planner_User__c=objTestUser2.Id;
                   insert objInvOwner1;
                   
                   Inventory_Owner__c objCheckInvOwner=[Select Id,Plant_Code__c,Material_Code__c,Plant_Material_Code__c FROM Inventory_Owner__c where Id=:objInvOwner1.Id LIMIT 1];
                   System.assertEquals(objCheckInvOwner.Plant_Material_Code__c,objCheckInvOwner.Plant_Code__c+objCheckInvOwner.Material_Code__c);
                   
                   Material_Plant_Batch__c objInventory1=new Material_Plant_Batch__c();
                   objInventory1.Plant__c=objTestPlant1.Id;
                   objInventory1.Material__c=objTestMaterial1.Id;
                   objInventory1.Name='Test Inventory1';
                   objInventory1.Plant_Code__c='1234';
                   objInventory1.Material_Number__c='6547';
                   insert objInventory1;
                   
                   Material_Plant_Batch__c objCheckInv1=[SELECT Id,Inventory_Owner__c from Material_Plant_Batch__c where Id=:objInventory1.Id LIMIT 1];
                   System.assertEquals(objCheckInv1.Inventory_Owner__c,objInvOwner1.Planner_User__c);
               }
             test.StopTest();
     
     }



}