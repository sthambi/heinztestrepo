/**
  * Apex Trigger: CallPlannerController
  * Description: Controller class for CallPlanner Page
  * Created By: Ashish Sharma (JDC)
  * Created Date: May 20th, 2014.
  */
public with sharing class CallPlannerController {
    public Call_Planner__c callPlanner{get; set;}
    public String accountId{get; set;}
    public String fireflyID{get; set;}
    String ownerId;
    String accName;

    public CallPlannerController(Apexpages.StandardController stdController){
        callPlanner = (Call_Planner__c) stdController.getRecord();
        accountId = Apexpages.currentPage().getParameters().get('AccountId');
        ownerId = Apexpages.currentPage().getParameters().get('OwnerId');
        accName = Apexpages.currentPage().getParameters().get('accountName');
        fireflyID = Apexpages.currentPage().getParameters().get('FireflyID');

        if(Apexpages.currentPage().getParameters().get('id') != null) {
            callPlanner = [Select name, Solo_Call__c, Account_Name__c,
                           Team_Call__c, Owner_Name__c, Team_Members__c, Contact__c,
                           Date_of_Call__c, Short_term_strategic_objective__c, Description_of_Call__c,
                           Results__c, New_Products_Introduced__c
                           from Call_Planner__c
                           where id = :Apexpages.currentPage().getParameters().get('id')];
        }
        if(accountId != null){
            callPlanner.Account_Name__c = accountId;
        }

        system.debug('========ownerId===='+ownerId);
        if(ownerId != null){
            callPlanner.Owner_Name__c = userInfo.getUserId();
        }

        system.debug('========callPlanner.Owner_Name__c===='+callPlanner.Owner_Name__c);

        if(accName != null){
            String currentDate = Date.today().format();
            callPlanner.Name = 'Call Planner-' + accName + '-' + currentDate;
        }

    }

    public Pagereference save(){
            try{
            if(callPlanner.Description_of_Call__c != null && callPlanner.Description_of_Call__c != '' && callPlanner.Results__c != null && callPlanner.Results__c != '') {
                callPlanner.Call_Work_Complete__c = true;
            }
            Boolean isUpdate = false;
            if(callPlanner.ID == null) {
                insert callPlanner;
            }else {
                update callPlanner;
                isUpdate = true;
            }
            
            Account acc = new Account();
            
            if(callPlanner.Account_Name__c != null) {
                acc = [Select name from Account where id = :callPlanner.Account_Name__c limit 1] ;
            }
            //upsert callPlanner;


            system.debug('====callPlanner======'+callPlanner);
            if(callPlanner.New_Products_Introduced__c != null) {
                system.debug('====callPlanner=2====='+callPlanner);
                Campaign cmp = [Select name from Campaign
                                where id= :callPlanner.New_Products_Introduced__c
                                limit 1];
                return new Pagereference('/' + '006/e?opp17='+cmp.name+'&opp4='+acc.name+'&opp3='+acc.name+'-'+cmp.name+'&opp11=New Opportunity');
            }else {
                if(isUpdate) {
                    return new pagereference('/'+acc.Id);
                }else {
                    String callPlannerDate = '';
                    String callPlannerStartTime = '';
                    String callPlannerEndTime = '';
                   // pagereference pg = new pagereference();
                    if(callPlanner.Date_of_Call__c != null) {
                        callPlannerDate = callPlanner.Date_of_Call__c.month()+'/'+callPlanner.Date_of_Call__c.day()+'/'+callPlanner.Date_of_Call__c.year();

                        if(callPlanner.Date_of_Call__c.hour() > 11){
                            callPlannerStartTime = (callPlanner.Date_of_Call__c.hour() -12)+ ':' + callPlanner.Date_of_Call__c.minute() ;
                            callPlannerEndTime = (callPlanner.Date_of_Call__c.hour() -12 + 1)+ ':' + callPlanner.Date_of_Call__c.minute() ;
                            callPlannerStartTime += ' PM';
                            callPlannerEndTime += ' PM';
                        }else{
                            callPlannerStartTime = (callPlanner.Date_of_Call__c.hour())+ ':' + callPlanner.Date_of_Call__c.minute() ;
                            callPlannerEndTime = (callPlanner.Date_of_Call__c.hour() + 1)+ ':' + callPlanner.Date_of_Call__c.minute() ;
                            callPlannerStartTime += ' AM';
                            callPlannerEndTime += ' AM';
                        }

                        String pmHrs = '1401994800000Test';
                        if(callPlanner.Date_of_Call__c.getTime() + 'Test' == pmHrs){
                            callPlannerStartTime = '12:00 PM';
                        }

                        String amHrs = '1401951600000Test';
                        if(callPlanner.Date_of_Call__c.getTime() + 'Test' == amHrs){
                            callPlannerStartTime = '12:00 AM';
                        }

                        if(callPlanner.Contact__c != null) {
                            Contact cnt = [Select name from Contact where id = :callPlanner.Contact__c limit 1];
                            return new Pagereference('/' + '00U/e?evt2='+cnt.name+'&what_id='+callPlanner.Id+'&retURL='+callPlanner.Id+'&StartDateTime='+EncodingUtil.urlEncode(callPlannerDate, 'UTF-8')+'&StartDateTime_time='+EncodingUtil.urlEncode(callPlannerStartTime, 'UTF-8')+'&EndDateTime='+EncodingUtil.urlEncode(callPlannerDate, 'UTF-8')+'&EndDateTime_time='+EncodingUtil.urlEncode(callPlannerEndTime, 'UTF-8')+'&evt5=Meeting with '+acc.name);
                         }else {
                            return new Pagereference('/' + '00U/e?what_id='+callPlanner.Id+'&retURL='+callPlanner.Id+'&StartDateTime='+EncodingUtil.urlEncode(callPlannerDate, 'UTF-8')+'&StartDateTime_time='+EncodingUtil.urlEncode(callPlannerStartTime, 'UTF-8')+'&EndDateTime='+EncodingUtil.urlEncode(callPlannerDate, 'UTF-8')+'&EndDateTime_time='+EncodingUtil.urlEncode(callPlannerEndTime, 'UTF-8')+'&evt5=Meeting with '+acc.name);
                         }
                    } else {
                        if(callPlanner.Contact__c != null) {
                            Contact cnt = [Select name from Contact where id = :callPlanner.Contact__c limit 1];
                            return new Pagereference('/' + '00U/e?evt2='+cnt.name+'&what_id='+callPlanner.Id+'&retURL='+callPlanner.Id+'&evt5=Meeting with '+acc.name);
                         }else {
                            return new Pagereference('/' + '00U/e?what_id='+callPlanner.Id+'&retURL='+callPlanner.Id+'&evt5=Meeting with '+acc.name);
                         }
                    }
                }
            }
            }

         catch(DMLException  e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, e.getDmlMessage(0)));
            return null;
        }
    }

}