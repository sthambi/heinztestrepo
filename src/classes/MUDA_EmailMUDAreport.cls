/**
 *    @author Ameya Hadkar
 *    @date   16/06/2015
      @description    Batch Job for Sending the Email which contains link to Risk Items report.
      Modification Log: 
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Ameya Hadkar                  16/06/2015            Original Version
 */
global class MUDA_EmailMUDAreport implements Database.Batchable<SObject>, Database.Stateful
{
    //getting Ids from Custom Settings
    String plannerProfileID = MUDA_App_Labels__c.getInstance('MUDA Planner Profile ID').Label_Value__c;
    String replyToAddressID = MUDA_App_Labels__c.getInstance('MUDA_Reply_To_Address').Label_Value__c;
    String emailTemplateID = MUDA_App_Labels__c.getInstance('MUDA Email Template').Label_Value__c;
    
    global MUDA_EmailMUDAreport()
    {        
        
    }
    
    global Database.queryLocator start(Database.BatchableContext ctx)
    {
        if(!Test.isRunningTest())
        {
             return Database.getQueryLocator([SELECT Email,FirstName,Id,IsActive,Name,ProfileId,Title FROM User WHERE IsActive = TRUE AND ProfileId = : plannerProfileID]);
        }
        else
        {
             return Database.getQueryLocator([SELECT Email,FirstName,Id,IsActive,Name,ProfileId,Title FROM User WHERE IsActive = TRUE AND ProfileId = : plannerProfileID LIMIT 1]);
        }
    }
    
    global void execute(Database.BatchableContext ctx, List<Sobject> scope)
    {        
        List<User> usrList = (List<User>)scope;    
        
        if(usrList.size()>0)
        {
            for(User u : usrList)
            {
                /***     Code TO Send EMail Alert     ***/

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                
                String[] toAddresses = new String[] { u.Email };
                // For Test // String[] toAddresses = new String[] { 'nimore@deloitte.com' }; //
                 
                
                mail.setToAddresses(toAddresses);
                //mail.setCcAddresses(ccAddresses);
                
                mail.setReplyTo(replyToAddressID);
                
                mail.setSenderDisplayName('MUDA Team');
                
                mail.setBccSender(false);
                
                mail.setUseSignature(false);
                
                mail.setTemplateId(emailTemplateID);
                
                mail.setTargetObjectId(u.Id);
                
                mail.setSaveAsActivity(false);
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
    }    
    
    global void finish(Database.BatchableContext ctx)
    {
        
    }
}