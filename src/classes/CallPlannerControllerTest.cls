/**
  * Apex Trigger: CallPlannerControllerTest
  * Description: Test Class for CallPlannerController
  * Created By: Ashish Sharma (JDC)
  * Created Date: May 20th, 2014.
  */
@isTest
private class CallPlannerControllerTest {
  @isTest
  private static void callPlanner(){ 
    Test.startTest();
        Apexpages.currentPage().getParameters().put('OwnerId', UserInfo.getUserId());
        Apexpages.Standardcontroller stdController = 
                                    new Apexpages.Standardcontroller(new Call_Planner__c());
        
        CallPlannerController controller = new CallPlannerController(stdController);
        
        System.assert(controller.callPlanner <> null);
    Test.stopTest();
  }
}