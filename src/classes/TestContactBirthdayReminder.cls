@isTest
private class TestContactBirthdayReminder {

    static testMethod void mainTest() {
        
		// variables
		date myDOB = date.today().addDays(7);
		String myEmail = 'TestCBR@salesforce.com';
        
        // Insert 1 new contact whose BD is 7 days from now
        Contact testContact = new Contact();
        testContact.FirstName = 'Test';
        testContact.LastName = 'Contact';
        testContact.Email = myEmail;
        testContact.Birthdate = myDOB;
        insert testContact;
        
        // START TEST
        test.startTest();
        
        // Call the main function with the contact having birthday
        ContactBirthdayReminder CBR = new ContactBirthdayReminder();
        CBR.setReminderFlag();
        
        // STOP TEST
        test.stopTest();
        
        // Compare the fields after running
        Contact c2 = [select Id, Birthdate, Next_Birthday__c, Birthday_Reminder_Send_Now__c
        				, Birthday_Reminder_Last_Sent__c
        				from Contact
        				where EMail = : myEmail
        				limit 1
        			  ];
        
        System.assert(!c2.Birthday_Reminder_Send_Now__c);
        System.assertEquals(c2.Birthdate, myDOB);
        System.assertEquals(c2.Next_Birthday__c, myDOB);
        System.assertNotEquals(c2.Birthday_Reminder_Last_Sent__c, null);
        
    }//end mainTest()
    
    
    static testMethod void cronTest() {
    	
        // Schedule the test job
        System.debug('Testing the Scheduled Job...');
        String CRON_EXP = '0 0 0 * * ?';
        String jobId = System.schedule('testContactBirthdayReminder', CRON_EXP, new ContactBirthdayReminder());
        
        // Get the information from the CronTrigger API object  
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                            FROM CronTrigger 
                            WHERE id = :jobId];
        
        // Verify the expressions are the same  
        System.assertEquals(CRON_EXP, ct.CronExpression);
    
        // Verify the job has not run  
        System.assertEquals(0, ct.TimesTriggered);
        
    }//end cronTest()
}