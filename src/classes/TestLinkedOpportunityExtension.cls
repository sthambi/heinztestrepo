/*********************************************************************************
 Author       :   Dipika (Appirio Offshore)
 Created Date :   June 18,2013
 Task         :   T-152839
 Description  :   Test class for LinkedOpportunityExtension class
*********************************************************************************/
@isTest
private class TestLinkedOpportunityExtension {
    
    //Test method to test functionality of LinkedOpportunityExtension class
    static testMethod void TestLinkedOpp(){
        list<Opportunity> listOpp = new list<Opportunity>([SELECT Id FROM Opportunity]);
        system.assertEquals(listOpp.size(),0);
        Account acc = createTestData();
        Opportunity opp = new Opportunity();
        ApexPages.currentPage().getParameters().put('AccountId', acc.Id);
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(opp);
        LinkedOpportunityExtension oppExtension = new LinkedOpportunityExtension(con);
        oppExtension.opp.Name = 'TestOpp';
        oppExtension.opp.of_Linked_Opportunities__c = '2';
        oppExtension.opp.CloseDate = Date.today();
        oppExtension.saveOpportunities();
        
        listOpp = new list<Opportunity>([SELECT Id FROM Opportunity]);
        system.assertEquals(oppExtension.opp.Name, 'TestOpp - Step 1');
        system.assertEquals(listOpp.size(),2);
    }
    
    //Create test data
    private static Account createTestData(){
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        return a;
    }
}