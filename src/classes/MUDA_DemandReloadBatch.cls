/**
 *    @author Nikhil More
 *    @date   26/05/2015
      @description    Batch Job for forecast assignment.
      Modification Log: 
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Nikhil More                  26/05/2015            Original Version
 */
global class MUDA_DemandReloadBatch implements Database.Batchable<SObject>, Database.Stateful
{
    // MUDA_Data_Reload__c dr = MUDA_Data_Reload__c.getInstance('Forecast Update');
    
    global MUDA_DemandReloadBatch()
    {        
        
    }
    
    global Database.queryLocator start(Database.BatchableContext ctx)
    {
        
        Date startDate;
        Date endDate;
        Date toDay = Date.today(); // This is a Sunday

        startDate = toDay.addDays(-6); // This has to be the Previous Monday
        endDate = toDay.addDays(1);
        
        /*
        dr.IsDataReload__c = true;
        update dr;
        */
        
        if(!Test.isRunningTest())
        {   
            /* // Code for Weekly Demand Batch which considers demand records between base period date and nearest revision date //
            
            return Database.getQueryLocator([SELECT Id, Base_Period_Date__c, Revision_Date__c FROM Demand_Forecast__c WHERE Base_Period_Date__c = : startDate AND Revision_Date__c = :endDate ]);
            
            // WHERE Forecast_Value__c > 0 - To Filter Demand Records based on non-zero forecast value
            
            */
            
            return Database.getQueryLocator([SELECT Base_Period_Date__c,Forecast_Value__c,Id,Material_Number__c, Material__c,Name,Plant_Code__c,Plant__c,Revision_Date__c FROM Demand_Forecast__c WHERE Forecast_Value__c >= 0 ORDER BY Forecast_Value__c DESC NULLS LAST]); 

        }
        else
        {
            return Database.getQueryLocator([SELECT Base_Period_Date__c,Forecast_Value__c,Id,Material_Number__c, Material__c,Name,Plant_Code__c,Plant__c,Revision_Date__c FROM Demand_Forecast__c WHERE Base_Period_Date__c = : startDate AND Revision_Date__c = :endDate LIMIT 1]);
        }
    }
    
    global void execute(Database.BatchableContext ctx, List<Sobject> scope)
    {        
        List<Demand_Forecast__c> dfList = (List<Demand_Forecast__c>)scope;    
                    
        if(dfList.size()>0)
        {
            //Database.update(dfList);
            
            MUDA_ForecastTriggerHelper.ForecastAssignmentMethod(dfList);
        }
    }    
    
    global void finish(Database.BatchableContext ctx)
    {
        /*
        dr.IsDataReload__c = false;
        
        update dr;
        */
    }
}