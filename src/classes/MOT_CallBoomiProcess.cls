/**
 *    @author Mohan Kalagatla
 *    @date   01/06/2015
 *    @description    Class to invoke Dell Boomi process from Salesforce
 *    Modification Log:
      ------------------------------------------------------------------------------------     
      Developer                       Date                Description
      ------------------------------------------------------------------------------------        
      Mohan Kalagatla                 01/06/2015            Original Version
 */

global class MOT_CallBoomiProcess {

   /*
   *  @author  Mohan Kalagatla 
   *  @description Method to invoke Dell Boomi process from Salesforce.
   *  @param   Id orderId
   *  @return  None
   */
@future (callout=true)
webservice static void boomiCallout(Id orderId){

// Instantiate a new HTTP request object 
HttpRequest req = new HttpRequest();

// Calling MOT_DellBoomiConnection.fetchDellBoomiConnection to populate HTTP request object  with Dell Boomi connection parameters
req = MOT_DellBoomiConnection.fetchDellBoomiConnection(req);

//Fetch the orders
//List<Order> orderList = [Select Id,OrderNumber,Account.Name,Requested_Delivery_Date__c,TotalAmount,OrderQuantity__c,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry,Distributor_Service_Rep__c,Distributor__c,(Select Id,Pricebookentry.Product2.Name,Pricebookentry.Product2.ProductCode,Product_Quantity__c,ListPrice,UnitPrice,Quantity from OrderItems) from  Order where Id = :orderId];
List<Order> orderList = [Select Id,OrderNumber,Account.FireFly_ID__c,Requested_Delivery_Date__c,Distributor__r.Name,Distributor__r.MOT_Distributor_ID__c,(Select Id,Pricebookentry.Product2.Name,Pricebookentry.Product2.ProductCode,Quantity,UnitPrice from OrderItems) from  Order where Id = :orderId];


//Convert Salesforce Order data to JSON format
String jsonString = JSON.serialize(orderList);

//Set Request Body
req.setBody(jsonString);
System.debug('JSON_STRING: '+jsonString);

// Instantiate a new http object
Http http = new Http();
try { 
        //Execute web service call here     
        HTTPResponse res = http.send(req);  
        
        //For debugging Response details
        System.debug('RESULT: '+res.toString());
        System.debug('STATUS: '+res.getStatus());
        System.debug('STATUS_CODE: '+res.getStatusCode());
        System.debug('RESULT_BODY: '+res.getBody());   
        
        //Throw exception if Status code of Response is not 200(Success)
        If(res.getStatusCode()!= 200){
        throw new myTestException(res.getBody());   
        }
} catch(Exception  ex) {
    System.debug('Exception: '+ex); 
    
    //Update Status and Error Message in Order object   
    for(Order ord :orderList){
    ord.Status='Submission Error';
    ord.Error_Message__c = ex.getMessage();
    }
    update orderList;
}
}

//Custom Exception class
public class myTestException extends Exception {}   
}