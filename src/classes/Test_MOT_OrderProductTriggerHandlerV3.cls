/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest//(seeAllData = true)
private class Test_MOT_OrderProductTriggerHandlerV3{
    static testMethod void OrderPrUniquePrdId() {
        //Only Heinz sales user
        user usrIU = TestDataUtility.createIntUserData();
        system.assert(usrIU.id!=null);
        System.runAs(usrIU ) {
        //Inserting ProfilePriceBookMapping__c custom setting
        TestDataUtility.creatPBCustomSettingData();
        //Accounts inserted
        List<Account> accLst = TestDataUtility.createAccountsData();
        system.assert(accLst!=null);
        
        ID standardPB = Test.getStandardPricebookID();
        system.assert(standardPB!=null);
        //insertion can only happen for defined price book MOT-US because of a trigger working for heinz user
        //Pricebook2 newPB = [select id from Pricebook2 where isStandard=false and isActive=true LIMIT 1];//and Name='MOT - US']; //'MOT - US' must for Heinz User
        
        Pricebook2 newPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert newPB;
        system.assert(newPB.id!=null);
        //List of Product inserted
        List<Product2> prodLst = TestDataUtility.createMOTProductsData();
                system.assert(prodLst!=null);
        //List of PriceBookentry        
        List<PricebookEntry> pricebkEntryList = new List<PricebookEntry>();
        PricebookEntry pbEntry1 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[0]);
        pricebkEntryList.add(pbEntry1);
        PricebookEntry pbEntry2 =TestDataUtility.createPricebookEntry(standardPB,newPB,prodLst[1]);
        pricebkEntryList.add(pbEntry2);
        system.assert(pricebkEntryList[1].id!=null);
        
        // order orderObj inserted.
        List<order> orderList = TestDataUtility.createOrdersData(accLst);
        //List<order> orderNewList = new List<Order>();
        for(order o : orderList){
        o.pricebook2id=newPB.id;
        //orderNewList.add(o);
        }
        update orderList;
        //orderList[1].pricebook2id=newPB.id;
        system.assert(orderList[0].id!=null);
        //OrderItem orItemObj inserted
        List<OrderItem> orItemList = TestDataUtility.createOrderItems(orderList,pricebkEntryList);
        system.assert(orItemList[1].id!=null);
       }
    }
}