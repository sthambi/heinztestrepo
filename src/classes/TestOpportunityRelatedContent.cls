/*********************************************************************************
 Author       :   Dipika (Appirio Offshore)
 Created Date :   April 16,2013
 Task         :   T-128538
 Description  :   test class for OpportunityRelatedContent class
                  
*********************************************************************************/
@isTest
private class TestOpportunityRelatedContent {
    
    //test data
    private static Opportunity createTestData(){
        Account acc = new Account(Name='testAcc');
        insert acc;
        
        Campaign campaignObj = new Campaign(Name = 'testCampaign');
        insert campaignObj;
        
        String fileData = 'Test file'; 
        Blob file = Blob.valueOf(fileData);
        
        Attachment att = new Attachment(Name = 'TestAtt',
                                        Body = file,
                                        ParentId = campaignObj.id);
        insert att;                             
        
        Opportunity opp = new Opportunity(Name = 'TestOpp',
                                          AccountId = acc.id,
                                          StageName = 'Qualify',
                                          CloseDate = Date.today(),
                                          campaignId = campaignObj.Id);
        insert opp;
        
        FeedItem feedItem = new FeedItem(); 
        feedItem.ContentFileName = 'test file';
        feedItem.ContentData = file;
        feedItem.ParentId = campaignObj.id;
        feedItem.Type = 'ContentPost';
        insert feedItem;
        
        return opp;
        
        
    }
    
    //test method for relatedContenVersionDocuments page
    static testMethod void oppRelatedContent() {
        Opportunity opp = createTestData();
        ApexPages.StandardController con = new ApexPages.StandardController(opp);
        OpportunityRelatedContent ORC = new OpportunityRelatedContent(con);
        system.assertEquals(ORC.feedItems.size(),1);
    }
}