/*
        * Developed by : Ankita
        *
        * Functionality : Automatically populate the pricebook to MOT-US for Heinz Sales Profile users
        *
        Date:       Developer:      Modification Description:
        06/01/2015    (Ankita)        created.
                                     
        */
    public with sharing class MOT_OrderTriggerHandler{
        public static void updatePriceBook(List<Order> lstNewOrder){
        String userId=UserInfo.getUserId();
        ProfilePriceBookMapping__c objProfilePriceBookMapping= ProfilePriceBookMapping__c.getInstance(userId);
            for (Order objNewOrd: lstNewOrder)
                {
                  objNewOrd.pricebook2id= objProfilePriceBookMapping.PriceBook__c;    //updates the Pricebookid for the profile Heinz sales User profile 
                }            
       }
    }