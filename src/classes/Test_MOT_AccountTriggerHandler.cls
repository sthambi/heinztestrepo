/***************************************************************************************************************************
*   Type        :   Test Class
*   Name        :   Test_MOT_AccountTriggerHandler 
*   Create By   :   Dipendu Chanda   - Deloitte USI
*   Description :   This class is the test class for MOT_AccountTriggerHandler and AccountTrigger
*   Modification Log:
*   --------------------------------------------------------------------------------------
*   * Developer                   Date          Description
*   * ------------------------------------------------------------------------------------                 
*   * Dipendu Chanda                5.06.2015      Created 
*****************************************************************************************************************************/
@isTest
private class Test_MOT_AccountTriggerHandler {
    static testMethod void updateMotProdOnDistributorTest() {
        //Only Heinz sales user 
        user usrHNZ = TestDataUtility.createUserData();
        system.assert(usrHNZ.id!=null);
        System.runAs(usrHNZ){
            //Distributor accounts inserted
            List<Account> accLst = TestDataUtility.createAccountsData();
                accLst[0].Account_Type__c='Distributor';
                accLst[1].Account_Type__c='Distributor';
                update accLst;
                system.assert(accLst!=null);
            //List of Product inserted
            List<Product2> prodLst = TestDataUtility.createMOTProductsData();
                system.assert(prodLst!=null);
            //Distributor product 1 and 2 inserted
            List<Distributor_Products__c> dpList = new List<Distributor_Products__c>();
            Distributor_Products__c dp1 = new Distributor_Products__c(  Account__c=accLst[0].id,
                                                                        Product__c=prodLst[0].id);
                dpList.add(dp1);
            Distributor_Products__c dp2 = new Distributor_Products__c(  Account__c=accLst[1].id,
                                                                        Product__c=prodLst[1].id);
                dpList.add(dp2);
                insert dpList;
                system.assert(dpList!=null);
            //Updating the distributor product 1.
            accLst[0].MOT_Distributor__c=true;
            accLst[0].MOT_Distributor_ID__c='test';
            accLst[1].MOT_Distributor__c=true;
            accLst[1].MOT_Distributor_ID__c='test';
            update accLst;
            accLst[0].MOT_Distributor__c=false;
            accLst[0].MOT_Distributor_ID__c='test';
            accLst[1].MOT_Distributor__c=false;
            accLst[1].MOT_Distributor_ID__c='test';
            update accLst;
       }
    }
}