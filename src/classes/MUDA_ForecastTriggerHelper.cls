/**
 *    @author Nikhil More
 *    @date   26/05/2015
      @description    US-0104, US-0107: Track Field History for Action Plans. If the strategy or status is changed, capture this for history report.
      Modification Log:
      ------------------------------------------------------------------------------------  
      Developer                       Date                Description      
      ------------------------------------------------------------------------------------      
      Nikhil More                  26/05/2015            Original Version
 */

Public Class MUDA_ForecastTriggerHelper
{

   /*
   *  @author  Nikhil More 
   *  @description: Demand Forecast Assignment Algorithm
   *  @param List<Action_Plan__c> lstActionPlanNew, Map<Id, Action_Plan__c> mapActionPlanOld
   *  @return  None.
   */
    
    Public Static List<Material_Plant_Batch__c> invListToUpdate = new List<Material_Plant_Batch__c>();
    
    public static void ForecastAssignmentMethod(List<Demand_Forecast__c> listOfNewDemand)
    {
        // Variables Declaration //
        
        Decimal weeklyDemand = 0.0;
        Decimal shipLifeRemaining = 0;
        Decimal AvgDailyDemand = 0;
        Decimal daysReqdToConsume = 0;
        Decimal noOfRemainingDays = 0;
        Decimal totalNoOfDaySpent = 0;
        Set<Id> setAllPlantIDs = new Set<Id>();
        Set<Id> setAllMaterialIDs = new Set<Id>();
        List<Plant__c> plantsFromDemandMap = new List<Plant__c>();
        String tempPlantMaterial = '';
        List<Material_Plant_Batch__c> tempInvList = new List<Material_Plant_Batch__c>();
        Map<Id, Material__c> mapOfMaterials = new Map<Id, Material__c>();
        Map<Id, Material_Plant_Batch__c> mapInvToUpdate = new Map<Id,Material_Plant_Batch__c>();
        Set<Id> setDemandPlantIDs = new Set<Id>();
        Set<Id> setMemberPlantIDs = new Set<Id>();
        Map<Id, Id> mapPlantVsDemandPlant = new Map<Id, Id>();
        Map<Id, Plant__c> mapOfMemberPlants = new Map<Id, Plant__c>();
        Map<Id, Plant__c> mapOfDemandPlants = new Map<Id, Plant__c>();
        Map<Id, Plant__c> mapOfForecastedMPs = new Map<Id, Plant__c>();
        Map<Id, Demand_Forecast__c> mapOfForecastedDemand = new Map<Id, Demand_Forecast__c>();
        List<Material_Plant_Batch__c> tempMPBList = new List<Material_Plant_Batch__c>();
        Map<String, Decimal> mapPlantMaterialVsForecastValue = new Map<String, Decimal>();
        Map<String, Decimal> mapDemandPlantMaterialVsForecastValue = new Map<String, Decimal>();
        List<Material_Plant_Batch__c> combinedInventoryList = new List<Material_Plant_Batch__c>();
        Map<String, Demand_Forecast__c> mapDemandPlantMaterialVsForecast = new Map<String, Demand_Forecast__c>();
        Map<String, List<Material_Plant_Batch__c>> mapDemandPlantMaterialVsInventory = new Map<String, List<Material_Plant_Batch__c>>();
        Map<Id, Set<Id>> mapOfDemandPlantvsInventoryIDs = new Map<Id, Set<Id>>();
        Map<Id, List<Material_Plant_Batch__c>> mapOfDemandPlantvsInventory = new Map<Id, List<Material_Plant_Batch__c>>();
        Map<Id, List<Material_Plant_Batch__c>> mapOfAllPlantsvsInventory = new Map<Id, List<Material_Plant_Batch__c>>();
        Map<Id, List<Material_Plant_Batch__c>> mapOfMemberPlantvsInventory = new Map<Id, List<Material_Plant_Batch__c>>();
        List<Material_Plant_Batch__c> demandPlantInventory = new List<Material_Plant_Batch__c>();
        List<Material_Plant_Batch__c> memberPlantInventory = new List<Material_Plant_Batch__c>();
        
        // Iterating the mapOfNewDemand //
        if(!listOfNewDemand.isEmpty())
        {    
            for(Demand_Forecast__c fd : listOfNewDemand)
            {
                setAllPlantIDs.add(fd.Plant__c);
                setAllMaterialIDs.add(fd.Material__c);
            }
        }
        // Gathering the Demand Plants and Member Plants //
        if(!setAllPlantIDs.isEmpty())
        {
            plantsFromDemandMap = [SELECT Demand_Plant__c,Id,Name, Plant_Code__c,Demand_Plant_Code__c FROM Plant__c WHERE Id IN : setAllPlantIDs];
            
            for(Plant__c pl : plantsFromDemandMap)
            {
                if(pl.Plant_Code__c == pl.Demand_Plant_Code__c)
                {
                    setDemandPlantIDs.add(pl.Id);
                }
                if(pl.Plant_Code__c != pl.Demand_Plant_Code__c)
                {
                    setMemberPlantIDs.add(pl.Id);
                }
            }
            
            if(!setMemberPlantIDs.isEmpty())
            {
                mapOfForecastedMPs =  new Map<Id, Plant__c>([SELECT Demand_Plant__c,Id,Name,
                                                                    Plant_Code__c,Demand_Plant_Code__c 
                                                                    FROM Plant__c 
                                                                    WHERE Id IN : setMemberPlantIDs]);
                    
                for(Plant__c mp : mapOfForecastedMPs.values())
                {
                    setDemandPlantIDs.add(mp.Demand_Plant__c);
                }
            }
            
            if(!setDemandPlantIDs.isEmpty())
            {
                mapOfDemandPlants = new Map<Id, Plant__c>([SELECT Demand_Plant__c,Id,Name,
                                                                  Plant_Code__c,Demand_Plant_Code__c 
                                                                  FROM Plant__c 
                                                                  WHERE Id IN : setDemandPlantIDs]);
                    
                mapOfMemberPlants = new Map<Id, Plant__c>([SELECT Demand_Plant__c,Id,Name,
                                                                  Plant_Code__c,Demand_Plant_Code__c 
                                                                  FROM Plant__c 
                                                                  WHERE Demand_Plant__c IN : setDemandPlantIDs]);
           }
      }
      
        // Preparing a Map of Plant ID vs Demand Plant ID
      if(!mapOfDemandPlants.isEmpty())
      {
          for(Id dp : mapOfDemandPlants.keyset())
          {
              if(!mapOfMemberPlants.isEmpty())
              {
                  for(Plant__c mp : mapOfMemberPlants.values())
                  {
                      if(mp.Demand_Plant__c == dp)
                      {
                          mapPlantVsDemandPlant.put(mp.Id, dp);
                      }
                  }
              }
              for(Plant__c mp : mapOfDemandPlants.values())
              {
                  if(mp.Id == dp)
                  {
                    mapPlantVsDemandPlant.put(mp.Id, dp);
                  }
              }
          }
      }

      if(!setAllMaterialIDs.isEmpty())
      {
          // Fetch Demand from Keyset of Plant Vs DemandPlant
          if(!mapPlantVsDemandPlant.isEmpty())
          {
              mapOfForecastedDemand = new Map<Id, Demand_Forecast__c>([SELECT Base_Period_Date__c,Forecast_Value__c,Id,Material_Number__c,
                                                                              Material__c,Name,Plant_Code__c,Plant__c,Revision_Date__c 
                                                                              FROM Demand_Forecast__c
                                                                              WHERE Material__c = : setAllMaterialIDs
                                                                              AND Plant__c = : mapPlantVsDemandPlant.keyset()]);
          }
          
          // Gather the List of Materials and form a Map of Material ID vs Material Number
          
          mapOfMaterials = new Map<Id, Material__c>([SELECT Id,Material_Number__c,Name FROM Material__c WHERE Id IN : setAllMaterialIDs]);
          
      }
      
        // Preparing Map of Plant Vs Forecast Records
        if(!mapOfForecastedDemand.isEmpty())
        {
            Decimal tempAggForecastValue = 0.0;
            String tempMemberPlantMaterialConcatenate = '';
            String tempDemandPlantMaterialConcatenate = '';
            
            for(Plant__c dp : mapOfDemandPlants.values())
            {
                for(Id mId : setAllMaterialIDs)
                {
                    for(Demand_Forecast__c fd : mapOfForecastedDemand.values())
                    {
                        if(mId == fd.Material__c && dp.Id == fd.Plant__c)
                        {
                            tempDemandPlantMaterialConcatenate = dp.Demand_Plant_Code__c + fd.Material_Number__c;
                            
                            mapDemandPlantMaterialVsForecastValue.put(tempDemandPlantMaterialConcatenate, fd.Forecast_Value__c);
                        }
                    }
                }
            }
           
            for(Plant__c mp : mapOfMemberPlants.values())
            {
                for(Id mId : setAllMaterialIDs)
                {
                    for(Demand_Forecast__c fd : mapOfForecastedDemand.values())
                    {
                        if(mId == fd.Material__c && mp.Id == fd.Plant__c)
                        {
                            tempDemandPlantMaterialConcatenate = mp.Demand_Plant_Code__c + fd.Material_Number__c;
                            
                            if(mapDemandPlantMaterialVsForecastValue.containsKey(tempDemandPlantMaterialConcatenate))
                            {
                                tempAggForecastValue = fd.Forecast_Value__c + mapDemandPlantMaterialVsForecastValue.get(tempDemandPlantMaterialConcatenate);
                            }
                            else
                            {
                                tempAggForecastValue = fd.Forecast_Value__c;
                            }
                            mapDemandPlantMaterialVsForecastValue.put(tempDemandPlantMaterialConcatenate, tempAggForecastValue);
                        }
                    }
                }
            }
        }
        
        if(!mapOfDemandPlants.isEmpty() && !setAllMaterialIDs.isEmpty())
        {
            // Getting the inventory items against the Demand Plant
                     
            demandPlantInventory = [SELECT Id, Name, Plant__c, Material__c, Plant_Code__c, Plant__r.Demand_Plant_Code__c,
                                                Demand_Plant__c, Material_Number__c, Corrected_Stop_Ship_Date__c,
                                                Best_Before_End_Date__c, Current_Week_DP_Forecast__c, Daily_Demand__c,
                                                Cumulative_Consumption__c, Demand_Assigned__c, Daily_Consumption__c,
                                                Quantity_Blocked__c, Quantity_Unrestricted__c, At_Risk__c,
                                                Quantity_At_Risk_of_Reaching_Trade_BBE__c
                                                FROM Material_Plant_Batch__c 
                                                WHERE Material__c IN : setAllMaterialIDs
                                                AND Plant__c IN : mapOfDemandPlants.keyset()
                                                AND Corrected_Stop_Ship_Date__c >= TODAY
                                                // AND Demand_Assigned__c = false - Process All Inventory
                                                ORDER BY Corrected_Stop_Ship_Date__c ASC NULLS LAST];
       }
       if(!mapOfMemberPlants.isEmpty() && !setAllMaterialIDs.isEmpty())
       {
            // Getting the Member Plant Inventory
                     
            memberPlantInventory = [SELECT Id, Name, Plant__c, Material__c, Plant_Code__c, Plant__r.Demand_Plant_Code__c,
                                                Demand_Plant__c, Material_Number__c, Corrected_Stop_Ship_Date__c,
                                                Best_Before_End_Date__c, Current_Week_DP_Forecast__c, Daily_Demand__c,
                                                Cumulative_Consumption__c, Demand_Assigned__c, Daily_Consumption__c,
                                                Quantity_Blocked__c, Quantity_Unrestricted__c, At_Risk__c,
                                                Quantity_At_Risk_of_Reaching_Trade_BBE__c
                                                FROM Material_Plant_Batch__c 
                                                WHERE Material__c IN : setAllMaterialIDs
                                                AND Plant__c IN : mapOfMemberPlants.keyset()
                                                AND Corrected_Stop_Ship_Date__c >= TODAY
                                                // AND Demand_Assigned__c = false - Process All Inventory
                                                ORDER BY Corrected_Stop_Ship_Date__c ASC NULLS LAST];
        }
        
        // Approach 2 - Use Maps for PlantVsInventory //
        
        if(!mapOfDemandPlants.isEmpty() && !demandPlantInventory.isEmpty())
        {
            for(Id dpId : mapOfDemandPlants.keyset())
            {
                tempMPBList.clear();
                
                for(Material_Plant_Batch__c mpb : demandPlantInventory)
                {
                    if(mpb.Plant__c == dpId)
                    {
                        if(mapOfDemandPlantvsInventory.containsKey(dpId))
                        {
                            for(Material_Plant_Batch__c mpbRec : mapOfDemandPlantvsInventory.get(dpId))
                            {
                                tempMPBList.add(mpbRec);
                            }
                        }
                        else
                        {
                            tempMPBList.add(mpb);
                        }
                    }
                }
                
                mapOfDemandPlantvsInventory.put(dpId, tempMPBList);
            }
        }
        
        if(!mapOfMemberPlants.isEmpty() && !demandPlantInventory.isEmpty())
        {
            for(Id mpId : mapOfMemberPlants.keyset())
            {
                tempMPBList.clear();
                
                for(Material_Plant_Batch__c mpb : memberPlantInventory)
                {
                    if(mpb.Plant__c == mpId)
                    {
                        if(mapOfMemberPlantvsInventory.containsKey(mpId))
                        {
                            for(Material_Plant_Batch__c mpbRec : mapOfMemberPlantvsInventory.get(mpId))
                            {
                                tempMPBList.add(mpbRec);
                            }
                        }
                        else
                        {
                            tempMPBList.add(mpb);
                        }
                    }
                }
                
                mapOfMemberPlantvsInventory.put(mpId, tempMPBList);
            }
        }
        
        if(!mapOfDemandPlantvsInventory.isEmpty())
        {
            for(Id pId : mapOfDemandPlants.keyset())
            {
                tempPlantMaterial = '';
                
                for(Id mId : setAllMaterialIDs)
                {
                    if(mapOfDemandPlants.containsKey(pId) && mapOfMaterials.containsKey(mId))
                    {
                        tempPlantMaterial = mapOfDemandPlants.get(pId).Demand_Plant_Code__c + mapOfMaterials.get(mId).Material_Number__c;
                    }
                    
                    if(mapDemandPlantMaterialVsForecastValue.containsKey(tempPlantMaterial))
                    {
                         weeklyDemand = mapDemandPlantMaterialVsForecastValue.get(tempPlantMaterial);
                    }
                    
                    if(mapOfDemandPlantvsInventory.containsKey(pId))
                    {
                    
                        shipLifeRemaining = 0;
                        AvgDailyDemand = 0;
                        daysReqdToConsume = 0;
                        noOfRemainingDays = 0;
                        totalNoOfDaySpent = 0;
                        
                        for(Material_Plant_Batch__c mpb : mapOfDemandPlantvsInventory.get(pId))
                        {
                            if(mpb.Material__c == mId)
                            {
                                if(weeklyDemand > 0)
                                {
                                    mpb.Current_Week_DP_Forecast__c = weeklyDemand;
                                            
                                    mpb.Daily_Demand__c = (mpb.Current_Week_DP_Forecast__c) / 5;
                                        
                                    if(mpb.Quantity_Blocked__c > 0)
                                    {
                                        mpb.At_Risk__c = TRUE;
                                    }
                                    if(mpb.Quantity_Blocked__c <= 0)
                                    {
                                        shipLifeRemaining = Date.today().daysBetween(mpb.Corrected_Stop_Ship_Date__c); // var x = input
    
                                        daysReqdToConsume = mpb.Quantity_Unrestricted__c / mpb.Daily_Demand__c;
                                            
                                        if(shipLifeRemaining > totalNoOfDaySpent)
                                        {
                                            noOfRemainingDays = shipLifeRemaining - totalNoOfDaySpent; // var w = x - z
                                        }
                                        if(shipLifeRemaining < totalNoOfDaySpent)
                                        {
                                            noOfRemainingDays = 0; // var w = x - z
                                        }
                                        if(noOfRemainingDays >= daysReqdToConsume)
                                        {
                                            mpb.At_Risk__c = FALSE;
                                            
                                            totalNoOfDaySpent = daysReqdToConsume + totalNoOfDaySpent;
                                                    
                                            mpb.Quantity_At_Risk_of_Reaching_Trade_BBE__c = 0;
                                                    
                                            mpb.Cumulative_Consumption__c = mpb.Quantity_Unrestricted__c;
                                                    
                                            mpb.Daily_Consumption__c = mpb.Daily_Demand__c;
                                            
                                        }
                                        if(noOfRemainingDays < daysReqdToConsume)
                                        {
                                            mpb.At_Risk__c = TRUE;
                                                    
                                            totalNoOfDaySpent = noOfRemainingDays + totalNoOfDaySpent; // var z = w + z
                                                    
                                            daysReqdToConsume = noOfRemainingDays; // y = w - the new y
                                                    
                                            mpb.Quantity_At_Risk_of_Reaching_Trade_BBE__c = mpb.Quantity_Unrestricted__c - (mpb.Daily_Demand__c * noOfRemainingDays);
                                                    
                                            mpb.Cumulative_Consumption__c = (mpb.Daily_Demand__c * noOfRemainingDays);
                                            
                                            mpb.Daily_Consumption__c = mpb.Daily_Demand__c;
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    mpb.Current_Week_DP_Forecast__c = 0;
                                            
                                    mpb.Daily_Demand__c = 0;
                                    
                                    if(mpb.Quantity_Blocked__c > 0)
                                    {
                                        mpb.At_Risk__c = TRUE;
                                    }
                                    if(mpb.Quantity_Unrestricted__c > 0)
                                    {
                                        mpb.At_Risk__c = TRUE;
                                    }
                                }
                                
                                mpb.Demand_Assigned__c = TRUE;
                                
                                mapInvToUpdate.put(mpb.Id, mpb);
                            }
                        }
                    }
                }
            }
        }
        
        if(!mapOfMemberPlantvsInventory.isEmpty())
        {
            for(Id pId : mapOfMemberPlants.keyset())
            {
                tempPlantMaterial = '';
                
                for(Id mId : setAllMaterialIDs)
                {
                    if(mapOfMemberPlants.containsKey(pId) && mapOfMaterials.containsKey(mId))
                    {
                        tempPlantMaterial = mapOfMemberPlants.get(pId).Demand_Plant_Code__c + mapOfMaterials.get(mId).Material_Number__c;
                    }
                    
                    if(mapDemandPlantMaterialVsForecastValue.containsKey(tempPlantMaterial))
                    {
                         weeklyDemand = mapDemandPlantMaterialVsForecastValue.get(tempPlantMaterial);
                    }
                    
                    if(mapOfMemberPlantvsInventory.containsKey(pId))
                    {
                        shipLifeRemaining = 0;
                        AvgDailyDemand = 0;
                        daysReqdToConsume = 0;
                        noOfRemainingDays = 0;
                        totalNoOfDaySpent = 0;
                        
                        for(Material_Plant_Batch__c mpb : mapOfMemberPlantvsInventory.get(pId))
                        {
                            if(mpb.Material__c == mId)
                            {
                                if(weeklyDemand > 0)
                                {
                                    mpb.Current_Week_DP_Forecast__c = weeklyDemand;
                                            
                                    mpb.Daily_Demand__c = (mpb.Current_Week_DP_Forecast__c) / 5;
                                        
                                    if(mpb.Quantity_Blocked__c > 0)
                                    {
                                        mpb.At_Risk__c = TRUE;
                                    }
                                    if(mpb.Quantity_Blocked__c <= 0)
                                    {
                                        shipLifeRemaining = Date.today().daysBetween(mpb.Corrected_Stop_Ship_Date__c); // var x = input
    
                                        daysReqdToConsume = mpb.Quantity_Unrestricted__c / mpb.Daily_Demand__c;
                                            
                                        if(shipLifeRemaining > totalNoOfDaySpent)
                                        {
                                            noOfRemainingDays = shipLifeRemaining - totalNoOfDaySpent; // var w = x - z
                                        }
                                        if(shipLifeRemaining < totalNoOfDaySpent)
                                        {
                                            noOfRemainingDays = 0; // var w = x - z
                                        }
                                        if(noOfRemainingDays >= daysReqdToConsume)
                                        {
                                            mpb.At_Risk__c = FALSE;
                                            
                                            totalNoOfDaySpent = daysReqdToConsume + totalNoOfDaySpent;
                                                    
                                            mpb.Quantity_At_Risk_of_Reaching_Trade_BBE__c = 0;
                                                    
                                            mpb.Cumulative_Consumption__c = mpb.Quantity_Unrestricted__c;
                                                    
                                            mpb.Daily_Consumption__c = mpb.Daily_Demand__c;
                                            
                                        }
                                        if(noOfRemainingDays < daysReqdToConsume)
                                        {
                                            mpb.At_Risk__c = TRUE;
                                                    
                                            totalNoOfDaySpent = noOfRemainingDays + totalNoOfDaySpent; // var z = w + z
                                                    
                                            daysReqdToConsume = noOfRemainingDays; // y = w - the new y
                                                    
                                            mpb.Quantity_At_Risk_of_Reaching_Trade_BBE__c = mpb.Quantity_Unrestricted__c - (mpb.Daily_Demand__c * noOfRemainingDays);
                                                    
                                            mpb.Cumulative_Consumption__c = (mpb.Daily_Demand__c * noOfRemainingDays);
                                            
                                            mpb.Daily_Consumption__c = mpb.Daily_Demand__c;
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    mpb.Current_Week_DP_Forecast__c = 0;
                                            
                                    mpb.Daily_Demand__c = 0;
                                    
                                    if(mpb.Quantity_Blocked__c > 0)
                                    {
                                        mpb.At_Risk__c = TRUE;
                                    }
                                    if(mpb.Quantity_Unrestricted__c > 0)
                                    {
                                        mpb.At_Risk__c = TRUE;
                                    }
                                }
                                
                                mpb.Demand_Assigned__c = TRUE;
                                
                                mapInvToUpdate.put(mpb.Id, mpb);
                            }
                        }
                    }
                }
            }
        }
        
        if(!mapInvToUpdate.isEmpty())
        {
            try
            {
                Database.SaveResult[] srList = Database.Update(mapInvToUpdate.values(), false);
                
                for (Database.SaveResult sr : srList)
                {
                    if (sr.isSuccess()) 
                    {
                        System.debug('Successfully Updated Inventory. Inventory ID: ' + sr.getId());
                    }
                    else 
                    {
                        for(Database.Error err : sr.getErrors()) 
                        {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        }
                    }
                }
                
                invListToUpdate.clear();
            }
            catch(Exception e)
            {
                System.Debug('An Error Occured while trying to update the mdpInventoryList : ' + e.getMessage());
            }
        }
    }
}