@isTest
// CallOutMock class in order to test HTTP callouts in MOT_CallBoomiProcess.Used in Test_MOT_CallBoomiProcess test class.
global class Test_MOT_CalloutMock implements HttpCalloutMock{
  global HttpResponse respond(HTTPRequest req){
    HttpResponse res = new HttpResponse();
    res.setStatus('OK');
    res.setStatusCode(200);
    res.setBody('success');
    return res;
  }
}