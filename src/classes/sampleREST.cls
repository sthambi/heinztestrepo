@RestResource(urlMapping='/test/*')
global with sharing class sampleREST {

    @HttpGet
    global static String getSampleRest() 
    {
	return 'Hello Test';
	}
}