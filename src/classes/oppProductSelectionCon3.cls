global class oppProductSelectionCon3 {
        public String searchStr{get;set;}
        public Integer prodIndex{get;set;}
        public  List<wrapProduct> wrapProductList{get;set;}
        public Boolean isAlreadySelected{get;set;}
        public Boolean isNotfirstLoad{get;set;}
        public Opportunity opp{get;set;}
        public string errorMsg{get;set;}
        public string prodId{get;set;}
        public string searchFilter{get;set;}
        public string pricebook2id{get;set;}
        public Boolean isEmpty{get;set;}
        Map<Id,PricebookEntry> masterProductMap;
        String retUrl;
        Id oppId;
        


        public oppProductSelectionCon3(ApexPages.StandardController controller) {
                System.debug('*******************************');
                errorMsg = 'ok';
                isEmpty=true;
                isNotfirstLoad=false;
                isAlreadySelected = false;
                prodIndex = null;
                wrapProductList = new List<wrapProduct>();
                oppId = controller.getId();
                retUrL = ApexPages.currentPage().getParameters().get('addTo');
                opp = [select pricebook2id,Name from Opportunity where Id=:oppId];
                if(opp.pricebook2id!=null){
                		//if pricebook is attached show products from the attached pricebook
                        pricebook2id =opp.pricebook2id;
                        masterProductMap=new Map<Id,PricebookEntry>([Select Id, UseStandardPrice, UnitPrice,Product2.Family, ProductCode, Product2Id,Product2.description, Pricebook2Id, Name,Pricebook2.name, IsActive, Product2.tier__c From PricebookEntry where Pricebook2id =:opp.pricebook2id]);
                }
                else{
                        masterProductMap=new Map<Id,PricebookEntry>([Select Id, UseStandardPrice, UnitPrice,Product2.Family, ProductCode, Product2Id,Product2.description, Pricebook2Id, Name,Pricebook2.name, IsActive, Product2.tier__c From PricebookEntry where Pricebook2.isStandard=true]);
                }
        }

        //Method to select product from auto suggest
        public void selectProduct(){           
                isAlreadySelected = false;

                PricebookEntry prod= new PricebookEntry();
                //retrieve the product from the master map
                prod = masterProductMap.get(prodId);
                System.debug(prodId+'================='+prod);
                if(prod !=null && (!prodAlreadySelected(prod.Id))){
                        wrapProduct wrap = new wrapProduct();
                        wrap.priceBookProd = prod;
                        wrap.oppLineItem.unitPrice = prod.unitPrice;
                        wrapProductList.add(wrap);

                }

                searchStr='';
                prodId = null;
                isEmpty = wrapProductList.isEmpty();
                

        }
        //method to remove product on onClick event
        public void removeProduct(){
                System.debug('##---------Remove------------'+prodIndex);
                isAlreadySelected = false;
                if(prodIndex!=null && prodIndex > -1 && prodIndex<=wrapProductList.size() && (!wrapProductList.isEmpty())){
                        wrapProductList.remove(prodIndex);
                }
                //check if any product is selected or not
                isEmpty = wrapProductList.isEmpty();
                prodIndex=null;

        }
        //method to save products
        public Pagereference saveProducts(){
                isNotfirstLoad = true;
                errorMsg = 'ok';
                integer i=1;
                //Check if any product is selected
                if(wrapProductList.isEmpty()){
                        //errorMsg = '<b>No Product Selected!</b><br/> Please select a product first.';
                        isEmpty = true;
                        return null;
                }


                System.debug('###--------ProdSave:oppId:'+oppId);
                Boolean skip=false;
                String searchStr='';
                String errorString='';
                errorMsg ='<b>Required field missing or not of required type.<br/>Please check following products</b><br/>';
                List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
                for(wrapProduct prod: wrapProductList){
                        //check for mandatory fields
                        if(prod.oppLineItem.Case_Volume__c==null ||prod.oppLineItem.Case_Volume__c==0){
                                errorMsg=errorMsg+i+'. '+prod.priceBookProd.Name+'<br/><b>&nbsp;&nbsp;Field:</b>'
                                                +((prod.oppLineItem.Case_Volume__c==null ||prod.oppLineItem.Case_Volume__c==0)?'Case Volume<br/>':'');
                                skip=true;
                               
                        }
                        if(!skip){
                                prod.oppLineItem.PricebookEntryId=prod.priceBookProd.Id;
                                prod.oppLineItem.OpportunityId=oppid;
                                // prod.oppLineItem.TotalPrice = prod.oppLineItem.unitPrice *  prod.oppLineItem.Quantity;
                                oppLineItemList.add( prod.oppLineItem);
                        }
                        ++i;
                }
                //if mandatory fields are present insert 
                if(!skip){
                        insert oppLineItemList;
                        errorMsg = 'ok';
                        return new Pagereference('/'+retUrl);
                }

                System.debug('###--------oppLineItemList'+oppLineItemList);
                return null;
        }
        //cancel method
        public Pagereference cancel(){
                return new Pagereference('/'+retUrl);
        }
        //the wrapper class
        public class wrapProduct{
                public PricebookEntry priceBookProd{get;set;}
                public OpportunityLineItem oppLineItem{get;set;} 

                public wrapProduct(){
                        priceBookProd = new PricebookEntry ();
                        oppLineItem = new OpportunityLineItem ();
                }
        }
        
        //Method to populate auto suggest field
        @RemoteAction
        global static list<PriceBookEntry> returnMatchingRec(String searchStr,String field2Search,String pricebook2id){
                String query;
                if(pricebook2id =='' || pricebook2id ==null){
                        query='Select Id, UseStandardPrice, UnitPrice,Product2.Family, ProductCode, Product2Id,Product2.description, Pricebook2Id, Name,Pricebook2.name, IsActive, Product2.Tier__c From PricebookEntry where '+field2Search+ ' Like '+'\'%'+searchStr+'%\' and Pricebook2.isStandard=true';
                }
                else{
                        query='Select Id, UseStandardPrice, UnitPrice,Product2.Family, ProductCode, Product2Id,Product2.description, Pricebook2Id, Name,Pricebook2.name, IsActive, Product2.Tier__c From PricebookEntry where '+field2Search+ ' Like '+'\'%'+searchStr+'%\' and pricebook2id ='+'\''+pricebook2id+'\' ';
                }

                list<PriceBookEntry> result = Database.query(query);
                System.debug('##########size##############'+result.size()+'####searchStr#####'+searchStr+'#####field2Search####'+field2Search+'##query###'+query);
                return result ;
        }
        //method to check whether the product is already added or not
        private Boolean prodAlreadySelected(String Id2Chk){
                for(wrapProduct wrap:wrapProductList){
                        if(wrap.priceBookProd.Id== Id2Chk){
                                isAlreadySelected = true;
                                return isAlreadySelected;
                        }
                }
                isAlreadySelected = false;        
                return isAlreadySelected ;
        }
}