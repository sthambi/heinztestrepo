/**
* Class Name   : TestScheduleAutoFollowScheduler Class
* Created By   : Kirti Agarwal(Appirio Offshore)
* Created Date : 24th April 2013
* Purpose      : test class for ScheduleAutoFollowScheduler
**/
@isTest 
public with sharing class TestScheduleAutoFollowScheduler {
 public static testmethod void unitTest(){
    try{
        ScheduleAutoFollowScheduler obj = new ScheduleAutoFollowScheduler();
        obj.ScheduleAutoFollowScheduler();
        test.startTest();
            ScheduleAutoFollowScheduler.start();
        test.stopTest();
    }catch(exception e){
        
    }
 }
}