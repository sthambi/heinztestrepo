/**
* Class Name   : UpdateStagingHistoryOwner
* Created By   : Dipika Gupta(Appirio Offshore)
* Created Date : 11th June 2013
* Purpose      : T-149805 batch class to update Stage History OwnerId
**/

global without sharing class UpdateStagingHistoryOwner implements Database.Batchable<sObject>{
	global final String Query;
	global final Date  todayDate;
	global final Date  nextDate;
   
   global UpdateStagingHistoryOwner(){
   	todayDate = Date.Today();
   	nextDate = Date.Today().addDays(1);
   	Query = 'SELECT ID, OwnerId, Owner_ID__c,CreatedDate FROM Stage_History__c Where CreatedDate >=: todayDate and CreatedDate <=: nextDate ';
   }
   
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
     list<String> listUserId = new list<String>();
     for(sobject StageHistory : scope){
     	if(StageHistory.get('Owner_ID__c')!=null){
     		listUserId.add((String)StageHistory.get('Owner_ID__c'));
     	}
     }
     map<Id,User> mapUser = new map<Id,User>([Select Id,isActive From User Where ID In : listUserId and isActive = true]);
     list<Stage_History__c> listStageHistory = new list<Stage_History__c>();
     
     for(sobject StageHistory : scope){
     	Stage_History__c SH = (Stage_History__c)StageHistory ;
     	if(SH.Owner_ID__c !=null){
     		if(mapUser.containsKey(SH.Owner_ID__c)){
     			SH.OwnerId = SH.Owner_ID__c; 
     			listStageHistory.add(SH);
     		}
     	}
     }
     update listStageHistory;
    }

   global void finish(Database.BatchableContext BC){
   }
}