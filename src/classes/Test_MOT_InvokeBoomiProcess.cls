/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
// Test Class for MOT_InvokeBoomiProcess,MOT_CallBoomiProcess and MOT_DellBoomiConnection classes
@isTest
private class Test_MOT_InvokeBoomiProcess {
    static testMethod void unitTest1() {
    
        //Only Heinz sales user
        user usrHNZ = TestDataUtility.createUserData();
        system.assert(usrHNZ.id!=null);
        System.runAs(usrHNZ ) {
        
        //Inserting Account        
        Account acc = TestDataUtility.createAccountData();
        acc.MOT_Distributor__c = true;
        acc.MOT_Distributor_ID__c = 'Labatt';
        update acc;
        system.assert(acc.id!=null);
        
        //Inserting Order
        order orderObj = new order( EffectiveDate = Date.today().addDays(-10), 
                                    Requested_Delivery_Date__c = Date.today().addDays(+10),
                                    Distributor__c = acc.id,
                                    Status = 'Draft',
                                    AccountId = acc.id,
                                    ShippingCity = 'Test',
                                    ShippingCountry = 'Test',
                                    ShippingPostalCode = '546666',
                                    ShippingState = 'Test',
                                    ShippingStreet = 'Test');
        insert orderObj;
        system.assert(orderObj.id!=null);
        
        //Inserting Dell_Boomi_Connection_Parameters__c custom setting
        Dell_Boomi_Connection_Parameters__c dbcp = new Dell_Boomi_Connection_Parameters__c();
        dbcp.Http_Method__c ='POST';
        dbcp.Webservice_URL__c='https://test.connect.boomi.com/ws/simple/getOrder';
        dbcp.Timeout__c=120000;
        dbcp.Username__c='SFDC@heinzintegration-6U2FOL.BKM1X1';
        dbcp.Password__c='98c89da8-a7ac-4297-9a5d-83fb36334cb8';
        dbcp.Name ='Dellboomi';
        insert dbcp;
        
        //Setting Mock class to Test_MOT_CalloutMock in order to test HTTP Callouts
        Test.setMock(HttpCalloutMock.class, new Test_MOT_CalloutMock());
        //Calling MOT_InvokeBoomiProcess class
        Test.startTest();
        MOT_InvokeBoomiProcess.boomiCallout(orderObj.Id);
        Test.stopTest();
       }
    }
}