/**
     *    @author Ankita Verma
     *    @date   04/06/2015
     *    @description    Controller to enable distributor Selection i.e. show the distributors which support the product selected by the distributor    
     *    Modification Log:
          ------------------------------------------------------------------------------------     
          Developer                       Date                Description
          ------------------------------------------------------------------------------------        
          Ankita Verma               04/06/2015            Original Version
     */
    public with sharing class MOT_distributorSelection{
        public List<Account> distributors {get; set;}
        public Account selectedDistributor {get; set;}
        public String orderid {get; set;}
        public Set<Id> distids = new Set<Id>();
        Map<Id,OrderItem> prodInOrder = new Map<Id,OrderItem>();
        List<Distributor_Products__c> dists = new List<Distributor_Products__c>();
        Map<Id,Set<Id>> acctProdMap = new Map<Id,Set<Id>>();
            /**
            * Constructor
            */      
            public MOT_distributorSelection(ApexPages.StandardController controller) {
           //Get the order id passed as parameter
            orderid = ApexPages.currentpage().getParameters().get('id');
            //Query  the order line items for the order id passed as parameter
            List<OrderItem> orderitems = [SELECT Id, Pricebookentry.Product2id, Quantity FROM OrderItem WHERE OrderItem.Orderid = :orderid];
            //Check if the orderItems list is not empty ,and if not empty put the productIds for the order Line items in the Map prodInOrder
            if(!orderitems.isEmpty()){
                for(orderitem o: orderitems){
                    prodInOrder.put(o.Pricebookentry.Product2id,o);
                }
            }   
            //Query Distributors from the Distributor_Products__c where product field on Distributor_Products__c is equal to the productId obtained in the map prodInOrder
            if(!prodInOrder.isEmpty()){
                dists = [SELECT Id, Account__c, Product__c, MOT_Order_Product_Limit__c FROM Distributor_Products__c where product__c in :prodInOrder.keySet()];
            }   
            //Loop through the list of distributor-product mapping to create a map of accounts with the set of available products
            if(!dists.isEmpty()){
                for(Distributor_Products__c dist:dists){
                    if(prodInOrder.containsKey(dist.Product__c) && prodInOrder.get(dist.product__c)!=null){
                        if(prodInOrder.get(dist.product__c).Quantity <= dist.MOT_Order_Product_Limit__c){
                            //if the acctProdMap is not empty ,for the accounts , produts are added in the set 
                            if(!acctProdMap.isEmpty()){
                                if(acctProdMap.containsKey(dist.Account__c) && acctProdMap.get(dist.Account__c) !=null){
                                   acctProdMap.get(dist.Account__c).add(dist.product__c);
                                }
                            //if the account is not found in the map, add the distributor-product mapping details in the map
                                else{
                                    set<Id> lstDist = new set<Id>();
                                    lstDist.add(dist.product__c);
                                    acctProdMap.put(dist.account__c,lstDist);
                                }
                            }
                            else{
                           //if the acctProdMap is empty, add the first distributor-product mapping details in the map
                                    set<Id> lstDist = new set<Id>();                  
                                    lstDist.add(dist.product__c);
                                    acctProdMap.put(dist.account__c,lstDist);
                                    
                            }   
                        }
                    }
                }
            }
             //Loop through map of account-product set and check if all products selected at the order items are present in the list of products for a distributor  
            if(!acctProdMap.isEmpty() && !prodInOrder.isEmpty()){           
                for(Id accId: acctProdMap.keyset()){
                //Check to validate if the distributor-product mapping contains all products selected at the order item.
                    if(acctProdMap.get(accId).containsAll(prodInOrder.keySet())){
                 //  match condition for product and distributor account. Populate the list of distributors where products match with the order item
                        distIds.add(accId);
                    }
                }    
           }
           //Querying the fields from the distributors to display them on VF.
            if(!distIds.isEmpty()){
                distributors = [SELECT Id, Name, BillingStreet,BillingCity,BillingState,Parent.name FROM Account WHERE Id IN :distids];
            }
            else{
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CannotSubmitDistributor));    
            }
             
        }
        //On click of Save selected distributor id being passed .
        public PageReference save() {
            Id selectedId = (Id)ApexPages.currentpage().getParameters().get('distId');
            for(Account d: distributors) {
                if(d.id == selectedId) {
                   selectedDistributor = d;
                    break;
                }
            }
            //If no distributor is selected on click of Save ,display an error message  
            if(selectedDistributor == null) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.selectdistributor));
                return null;
            }     
            //updating the order recordtype.
            Order orderToUpdate = [SELECT Distributor__c FROM Order WHERE id = :orderid];
            orderToUpdate.Distributor__c = selectedDistributor.id;
            list<recordtype> lstRecTypes = [select developername,id from recordtype where sobjectType = 'Order' and developername =:Label.Pending];
            if(!lstRecTypes.isEmpty()){
                orderToUpdate.recordTypeid= lstRecTypes[0].id;
            } 
            try
            {   
                update orderToUpdate;
                PageReference orderpage = new PageReference('/'+orderToUpdate.id);
                return orderpage;               
             } 
            catch(DmlException e){
                System.debug('An unexpected error has occurred: ' + e.getMessage());
             }
             
            return null;
        }
        //cancel functionality 
        public PageReference cancel() {
            return new PageReference('/'+orderid);
        }
      
    }