/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = true)
private class Test_MOT_OrderProductTriggerHandler_V1 {
    static testMethod void OrderPrUniquePrdId() {
        //Only Integration User
        Profile p = [select name, id from profile where name= 'Integration User']; 
        String testUserName = String.valueOf(System.now().getTime()) + '@hTest.com';
        User usrIA = new User( alias = 'TestUser', email='Newtest@email.com',
                                emailencodingkey='UTF-8', lastname='TestUser1', languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Denver', username=testUserName, isActive=true);
        Database.insert(usrIA);
        system.assert(usrIA.id!=null);
        System.runAs(usrIA) {
        //Inserting ProfilePriceBookMapping__c custom setting
        TestDataUtility.creatPBCustomSettingData();
        //Inserting Account
        Account acc = TestDataUtility.createAccountData();
        system.assert(acc.id!=null);
        //Inserting Campaign
        Campaign campaignObj = TestDataUtility.createCampaignData();
        system.assert(campaignObj.id!=null);
        
        ID standardPB = Test.getStandardPricebookID();
        system.assert(standardPB!=null);
        //insertion can only happen for defined price book
        Pricebook2 newPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert newPB;
        system.assert(newPB.id!=null);
        system.assert(newPB.isStandard==false);
        List<Product2> prodLst = new List<Product2>();
        //Product prod1 inserted
        String testName = String.valueOf(System.now().getTime()) + 'Test';
        Product2 prod1 = new Product2(  Name= testName+ 'Product',
                                        ProductCode='TestCode',
                                        IsActive=true);
            prodLst.add(prod1);
        //Product prod2 inserted
        Product2 prod2 = new Product2(  Name= testName+ 'Product',
                                        ProductCode='TestCode',
                                        IsActive=true);
            prodLst.add(prod2);
        insert prodLst;
        system.assert(prodLst[0].id!=null);
        PricebookEntry pbEntry = new PricebookEntry();
            pbEntry.pricebook2Id = standardPB;
            pbEntry.product2id = prodLst[0].id;
            pbEntry.unitprice = 1250.0;
            pbEntry.isactive = true;
        insert pbEntry;
        
        system.assert(pbEntry.id!=null);
        
        order orderObj = new order( EffectiveDate = Date.today().addDays(-10), 
                                    Requested_Delivery_Date__c = Date.today().addDays(+10),
                                    Status = 'Draft',
                                    Campaign__c= campaignObj.Id,
                                    AccountId = acc.id,
                                    ShippingCity = 'Test',
                                    ShippingCountry = 'Test',
                                    ShippingPostalCode = '546666',
                                    ShippingState = 'Test',
                                    Pricebook2Id = newPB.id,
                                    ShippingStreet = 'Test');
        insert orderObj;
        system.assert(orderObj.id!=null);
        OrderItem orItemObj = new OrderItem(OrderId = orderObj.id,
                                            PricebookEntryId = pbEntry.id,
                                            Quantity = 2,
                                            UnitPrice=100, 
                                            ServiceDate=Date.Today());
        insert orItemObj;
        system.assert(orItemObj.id!=null);
       }
    }
}