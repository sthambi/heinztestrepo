/**
 *    @author Mohan Kalagatla
 *    @date   01/06/2015
 *    @description    Class to fetch Dell Boomi Connection parameters
 *    Modification Log:
      ------------------------------------------------------------------------------------     
      Developer                       Date                Description
      ------------------------------------------------------------------------------------        
      Mohan Kalagatla                 05/06/2015            Original Version
 */

public class MOT_DellBoomiConnection {

   /*
   *  @author  Mohan Kalagatla 
   *  @description Method to fetch Dell Boomi Connection parameters
   *  @param None
   *  @return  None
   */

public static HttpRequest fetchDellBoomiConnection(HttpRequest req){

/* req.setMethod('POST');
req.setEndpoint('https://test.connect.boomi.com/ws/simple/getOrder'); 
req.setTimeout(120000);
String username = 'SFDC@heinzintegration-6U2FOL.BKM1X1';
String password = '98c89da8-a7ac-4297-9a5d-83fb36334cb8'; */

// Accessing Dell_Boomi_Connection_Parameters__c custom setting
Dell_Boomi_Connection_Parameters__c dbcp = Dell_Boomi_Connection_Parameters__c.getValues('Dellboomi');

//Setting the HttpRequest object with Dell Boomi Connection Parameters like Method,Endpoint,TimeOut,Authorization details
req.setMethod(dbcp.Http_Method__c);
req.setEndpoint(dbcp.Webservice_URL__c); 
req.setTimeout(Integer.valueOf(dbcp.Timeout__c));

String username = dbcp.Username__c;
String password = dbcp.Password__c;
Blob headerValue = Blob.valueOf(username + ':' + password);
//Preparing Authorization Header
String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
req.setHeader('Authorization', authorizationHeader);

//Returning populated HttpRequest object
return req;
}   
}