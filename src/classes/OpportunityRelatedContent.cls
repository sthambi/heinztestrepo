/*********************************************************************************
 Author       :   Dipika (Appirio Offshore)
 Created Date :   April 9,2013
 Task         :   T-128538
 Description  :   Controller class for OpportunityRelatedContent page
                  
*********************************************************************************/
public class OpportunityRelatedContent {
    public Opportunity opp{get;set;}
    public List<ContentVersion> relatedContenVersionDocuments{get;set;}
    public List<FeedItem> feedItems{get;set;}
    public String selectedContent{get;set;}
    
    //constructor
    public OpportunityRelatedContent(ApexPages.StandardController controller){
        opp = (Opportunity)controller.getRecord();
        opp = [SELECT ID,CampaignId From Opportunity Where Id=:opp.id];
        prepareFeedItemList();
    }
    
    /*&preparing list of related contents
    public void prepareContentVersionList(){
        relatedContenVersionDocuments = new List<ContentVersion>();
        for(ContentVersion version : [Select c.ID,c.Title, c.LastModifiedDate,ownerId, 
                                      c.CreatedDate,owner.name,contentDocumentId,contentDocument.Title 
                                      From ContentVersion c  
                                      where campaign__c = :opp.CampaignId 
                                      and isLatest = true and campaign__c!=null order by Title]){
            relatedContenVersionDocuments.add(version);
        }
    }*/
    
    
    //preparing list of related attachment
    public void prepareFeedItemList(){
        FeedItems = new List<FeedItem>();
        for(FeedItem att : [Select Id ,ParentId, Title, LastModifiedDate,
                              CreatedDate, RELATEDRECORDID From FeedItem 
                              where ParentId = :opp.CampaignId  
                              and ParentId!=null 
                              and Type = 'ContentPost' order by Title]){
            FeedItems.add(att);
        }
    }
}