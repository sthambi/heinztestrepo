/*
        * Developed by : Dipendu
        *
        * Functionality : Automatically populate the Product ID text field with the product value before insert.
        *
        Date:       Developer:      Modification Description:
        6/09/2015    (Dipendu)        created.
                                     
        */
public with sharing class MOT_DistrProdTriggerHandler{
        public static void FillProdId(List <Distributor_Products__c> dpItem){
                for (Distributor_Products__c a : dpItem) {
                        //to populate the text field"Product_ID__c" with its unique product ID
                        a.Product_ID__c=a.Product__c;
                }           
       }
}