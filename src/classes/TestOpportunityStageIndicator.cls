/**
* Class Name   : TestOpportunityStageIndicator Class
* Created By   : Kirti Agarwal
* Created Date : 24th April 2013
* Purpose      : test class for opportunityStageIndicator
**/
@isTest
public with sharing class TestOpportunityStageIndicator {
 
 static testMethod void unitTest(){
            Account a = new Account();
            a.Name = 'Test Account';
            insert a;
            
            Account a1 = new Account();
            a1.Name = 'Test Account';
            insert a1;
            
            Opportunity o = new Opportunity();
            o.Name = 'Test Opportunity';
            o.AccountId = a.Id;
            o.StageName = 'Closed Lost';
            o.CloseDate = system.today();
            insert o;
            
            ApexPages.StandardController cntroller = new ApexPages.StandardController(o);
            opportunityStageIndicator obj = new opportunityStageIndicator(cntroller);
            List<opportunityStageIndicator.wrapperStage> listOfwrapperStage = obj.lstStages;
            
            for(opportunityStageIndicator.wrapperStage wrapStage: listOfwrapperStage){
                system.assertEquals(wrapStage.strStageName , 'Closed Lost');
            }
             
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Opportunity 1';
            opp.AccountId = a.Id;
            opp.StageName = 'Proposal';
            opp.CloseDate = system.today();
            insert opp;
            
            cntroller = new ApexPages.StandardController(opp);
            obj = new opportunityStageIndicator(cntroller);
            opportunityStageIndicator.wrapperTask objTask = new opportunityStageIndicator.wrapperTask('test', 'test',true);
                String tName = objTask.strTaskName;
                String fName = objTask.strFieldName;
                Boolean isSelected = objTask.bolSelected;
            
  }
             
            
}