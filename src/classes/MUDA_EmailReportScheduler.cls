global class MUDA_EmailReportScheduler implements Schedulable
{
/**
 *    @author Ameya Hadkar
 *    @date   16/06/2015
      @description    Batch Job Scheduler Class for Scheduling the Email on Monday Morning for Risk Items Assigned to Planner.
      Modification Log: 
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Ameya Hadkar                  16/06/2015            Original Version
**/

    /* Code to Test the Scheduled Apex Class 
    
    MUDA_EmailMUDAreport mdr = new MUDA_EmailMUDAreport();
    ID batchprocessid = Database.executeBatch(mdr);
    
    MUDA_EmailReportScheduler ers = new MUDA_EmailReportScheduler();
    String sch = '0 40 2 * * ?';
    String jobID = system.schedule('Merge Job', sch, ers);
    
    */
    
    global void execute(SchedulableContext sc)
    {
        MUDA_EmailMUDAreport emrBatch = new MUDA_EmailMUDAreport();
        ID batchprocessid = Database.executeBatch(emrBatch,50);           
    }
}