/**
 *    @author Mohan Kalagatla
 *    @date   23/09/2015
      @description  User story US-1402: Capturing counter for open Action Plans
      Modification Log:
      ------------------------------------------------------------------------------------       
      Developer                       Date                Description
      ------------------------------------------------------------------------------------        
      Mohan Kalagatla                  23/09/2015            Original Version
 */

public class CUTS_ActionPlanTriggerHelper
{
   /*
   *  @author  Mohan Kalagatla 
   *  @description User story US-1402: Capturing counter for open Action Plans
   *  @param List<Action_Plan__c> lstActionPlanNew
   *  @return  None.
   */

    public static void captureActionPlanCount(List<Action_Plan__c> lstActionPlanNew)
    {
        List <Plant_Material__c> listPlantMaterials = new List<Plant_Material__c>();
        Set <Id> setPlantMaterials = new Set<Id>();
        Map<Id, Integer> mapPlantMaterialvsCountPrimary = new Map<Id, Integer>();
        Map<Id, Integer> mapPlantMaterialvsCountSecondary = new Map<Id, Integer>();
        List<aggregateResult> resultsPrimary = new List<aggregateResult>();
        List<aggregateResult> resultsSecondary = new List<aggregateResult>();
        try{
            for(Action_Plan__c apn : lstActionPlanNew){
                    setPlantMaterials.add(apn.Plant_Material__c);       
            }
            
            resultsPrimary = [Select Count(Id) apCount,Plant_Material__c pmID
                            from Action_Plan__c
                            WHERE Plant_Material__c in :setPlantMaterials 
                            AND Action_Plan_Type__c = :System.Label.CUTS_Primary_Action_Plan_Type
                            AND Status__c!= :System.Label.CUTS_Status_Closed
                            GROUP BY Plant_Material__c]; 
                            
            resultsSecondary = [Select Count(Id) apCount,Plant_Material__c pmID
                            from Action_Plan__c
                            WHERE Plant_Material__c in :setPlantMaterials 
                            AND Action_Plan_Type__c = :System.Label.CUTS_Secondary_Action_Plan_Type
                            AND Status__c!= :System.Label.CUTS_Status_Closed
                            GROUP BY Plant_Material__c];                
                            
            If(!resultsPrimary.isempty()){
                    for(AggregateResult ar : resultsPrimary){
                        mapPlantMaterialvsCountPrimary.put((Id) ar.get('pmID'), (Integer) ar.get('apCount'));
                    }
            }
            If(!resultsSecondary.isempty()){
                    for(AggregateResult ar : resultsSecondary){
                        mapPlantMaterialvsCountSecondary.put((Id) ar.get('pmID'), (Integer) ar.get('apCount'));
                    }
            }
                
            If(!mapPlantMaterialvsCountPrimary.isEmpty()){
                setPlantMaterials.addAll(mapPlantMaterialvsCountPrimary.keyset());
            }
            If(!mapPlantMaterialvsCountSecondary.isEmpty()){
                    setPlantMaterials.addAll(mapPlantMaterialvsCountSecondary.keyset());
            }
                
            listPlantMaterials = [SELECT Id, Open_Primary_Action_Plans__c,Open_Secondary_Action_Plans__c 
                                            FROM Plant_Material__c WHERE ID IN :setPlantMaterials]; 
                                            
            for(Plant_Material__c pm : listPlantMaterials){
                    pm.Open_Primary_Action_Plans__c = mapPlantMaterialvsCountPrimary.get(pm.Id);
                    pm.Open_Secondary_Action_Plans__c = mapPlantMaterialvsCountSecondary.get(pm.Id);
            }
                
                if(!listPlantMaterials.isEmpty()){
                        UPDATE listPlantMaterials;
                }
        }   
        catch(Exception e){
            MUDA_ErrorHandler.logErrorMethod(e.getMessage(),'Error','CUTS_ActionPlanTriggerHelper','captureActionPlanCount','MUDA_ActionPlanTrigger','NA','Action Plan','NA',UserInfo.getUserID(),'NA','Update',listPlantMaterials.size(),System.now(),'NA');
        }
    }
}