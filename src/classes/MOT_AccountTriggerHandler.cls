/*
        * Developed by : Dipendu
        *
        * Functionality : Automatically enable all MOT products for a new MOT distributor
        *
        Date:       Developer:      Modification Description:
        6/4/2015    (Dipendu)        created
        */
public with sharing class MOT_AccountTriggerHandler {
    public static void updateMotProdOnDistributor(List < Account > lstAcc) {
        Set < Id > distributorIds = new Set < Id > ();
        Set < Id > dProdExistIds = new Set < Id > ();
        Set < Id > prodIds = new Set < Id > ();
        List < Distributor_Products__c > listDprodAdd = new List < Distributor_Products__c > ();
        List < Distributor_Products__c > listDprodDelete = new List < Distributor_Products__c > ();
        //querying all the active MOT products.
        List < Product2 > listmotProd = [   SELECT Id, Name 
                                            FROM Product2 
                                            where isActive = true And MOT_Product__c = true  Order By Name 
                                            Limit 1000];
        Try {
            for (Product2 objProduct: listmotProd) {
                //storing all the product ids in a set prodIds.
                prodIds.add(objProduct.id);
            }
            for (Account objAcc: lstAcc) {
                //for all the account records which is modified we are storing it in distributorIds.
                distributorIds.add(objAcc.id);
            }
            //Creating list of account with related Distributor product values for the accounts being modified.
            List < Account > accDp = [  SELECT Id, Account_Type__c, MOT_Distributor__c, (   SELECT Id, Product__c, Account__c 
                                                                                            FROM Distributor_Products1__r) 
                                        FROM Account 
                                        where Id in : distributorIds];
            if(!accDp.isEmpty()){
                for (Account objAcc: accDp) {
                    //getting new and old value of the account record.
                    Account accNewTemp = (Account) objAcc;
                    Account accOldTemp = (Account) Trigger.oldMap.get(accNewTemp.Id);
                    System.Debug('******accNewTemp******' + accNewTemp.MOT_Distributor__c);
                    System.Debug('******accOldTemp******' + accOldTemp.MOT_Distributor__c);
                    //checking if the MOT checkbox has been changed and checked.
                    if (objAcc.Account_Type__c == 'Distributor' && accNewTemp.MOT_Distributor__c == true && accOldTemp.MOT_Distributor__c != true) {
                        System.Debug('******inside loop 1******');
                        //iterating over the already present distributor product under this distributor.
                        for (Distributor_Products__c dpAc: objAcc.Distributor_Products1__r) {
                            //checking if those distributor product already has MOT products.
                            if (prodIds.contains(dpAc.Product__c)) {
                                //Adding those distributor product which has MOT product to a list dProdExistIds.
                                dProdExistIds.add(dpAc.Product__c);
                            }
                        }
                        //Creating a new list to store the ids of the MOT products which are missing from the distributor.
                        Set < Id > tempsetId = prodIds;
                        tempsetId.removeAll(dProdExistIds);
                        // for missing products under that distributor creating distributor products.
                        if(!tempsetId.isEmpty()){
                            for (Id motId: tempsetId) {
                                Distributor_Products__c dpNew = new Distributor_Products__c();
                                dpnew.Account__c = objAcc.id;
                                dpnew.Product__c = motId;
                                //adding all the distributor products in a list to add it in a bulk together.
                                listDprodAdd.add(dpnew);
                                System.Debug('******listDprodAdd******' + listDprodAdd);
                            }
                        }
                    }
                    //checking if the MOT checkbox has been changed and unchecked.
                    if (objAcc.Account_Type__c == 'Distributor' && accNewTemp.MOT_Distributor__c == false && accOldTemp.MOT_Distributor__c != false) {
                        System.Debug('******inside loop 2******');
                        //iterating over the already present distributor product under this distributor.
                        for (Distributor_Products__c dpAc: objAcc.Distributor_Products1__r) {
                            //checking if those distributor product already has MOT products for the distributor.
                            if (prodIds.contains(dpAc.Product__c)) {
                                //adding all the distributor products in a list to delete it in a bulk together.
                                listDprodDelete.add(dpAc);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            //Catching any exception that occurs.
            System.Debug('There was an error ' + e.getMessage());
        }
        Try {    
            //check if the listDprodAdd is not empty before inserting
            if (!listDprodAdd.isEmpty()) {
                insert listDprodAdd;
                System.Debug('******listDprodAdd******' + listDprodAdd);
            }
        } catch (DMLException e) {
            //Catching any exception that occurs.
            System.Debug('There was an error ' + e.getMessage());
        }
        Try {
            //check if the listDprodDelete is not empty before deleting
            if (!listDprodDelete.isEmpty()) {
                delete listDprodDelete;
            }
        } catch (DMLException e) {
            //Catching any exception that occurs.
            System.Debug('There was an error ' + e.getMessage());
        }
    }
}