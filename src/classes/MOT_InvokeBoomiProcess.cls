/**
 *    @author Mohan Kalagatla
 *    @date   12/06/2015
 *    @description    Class to call MOT_CallBoomiProcess class so as to invoke Dell Boomi process from Salesforce
 *    Modification Log:
      ------------------------------------------------------------------------------------     
      Developer                       Date                Description
      ------------------------------------------------------------------------------------        
      Mohan Kalagatla                 12/06/2015            Original Version
 */
global class MOT_InvokeBoomiProcess{

   /*
   *  @author  Mohan Kalagatla 
   *  @description Method to call MOT_CallBoomiProcess class so to invoke Dell Boomi process from Salesforce.
   *  @param   Id orderId
   *  @return  None
   */
webservice static void boomiCallout(Id orderId){

//Calling MOT_CallBoomiProcess class to invoke Dell Boomi process
MOT_CallBoomiProcess.boomiCallout(orderId);
}
}