/**
* Class Name   : TestOpportunityTriggerHandler Class
* Created By   : Kirti Agarwal(Appirio Offshore)
* Created Date : 24th April 2013
* Purpose      : test class for OpportunityTriggerHandler
**/
@isTest
public with sharing class TestOpportunityTriggerHandler {
	static testMethod void unitTest(){
		
		// create map of Oppty recordtypes
        String OPPTY_LTO_RECTYPE = 'Opportunity_LTO_Layout';
		List<RecordType> listOfRecType = new List<RecordType>(
                                    [Select r.SobjectType, r.DeveloperName, r.Id 
                                     From RecordType r
                                     where  r.DeveloperName in ('Opportunity',:OPPTY_LTO_RECTYPE) ]);
                                     
        map<String,id> mapOfRecType = new map<String,id> ();

        for(RecordType rec : listOfRecType){
            mapOfRecType.put(rec.DeveloperName,rec.id);
		}           
        
        // Insert a new Oppty (will be LTO type)
        Opportunity o = createTestData();
        
        // confirm the recordtype is LTO
		Opportunity opp = [select id,RecordTypeId from Opportunity where id =: o.id];
        system.assertEquals(opp.RecordTypeId, mapOfRecType.get(OPPTY_LTO_RECTYPE) );

        // Update the Oppty to make it non-LTO rectype
        o.LTO_K_12__c = false;
        update o;
        
        // confirm the recordtype has changed to non-LTO
		opp = [select id,RecordTypeId from Opportunity where id =: o.id];
		system.assertEquals(opp.RecordTypeId,mapOfRecType.get('Opportunity'));
        
        // Update the Oppty again to make it LTO rectype again
        o.LTO_K_12__c = true;
        update o;
        
        // confirm the recordtype has changed to LTO
        opp = [select id,RecordTypeId from Opportunity where id =: o.id];
		system.assertEquals(opp.RecordTypeId,mapOfRecType.get(OPPTY_LTO_RECTYPE));

	} //end unitTest()
	
     
	private static Opportunity createTestData(){
		Account a = new Account();
		a.Name = 'Test Account';
		insert a;
		
		Opportunity o = new Opportunity();
		o.Name = 'Test Opportunity';
		o.AccountId = a.Id;
		o.StageName = 'Proposal';
		o.CloseDate = system.today();
		o.StartDate__c = system.today();
		o.EndDate__c = null;
		o.LTO_K_12__c = true;
		insert o;
		return o;
	} //end createTestData()


    private static User createUser(String profile)
    {
        Profile p = [SELECT Id FROM profile WHERE name=:profile]; 
        
        User testUser = new User(alias = 'newUser', email='newuser@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', 
            username='newuser@testorg.com'+String.valueOf(System.now().second() + System.now().millisecond()));
        
        insert testUser;
        return testUser;
    } //end createUser()
    
    
    //test Method called for before insert, Update
    //Added by Dipika Gupta (T-132472)
    private static testMethod void testBeforeInsertUpdate(){
        Opportunity opp = createTestData();
        
        //Confirm after insert that the OwnerId was copied to Owner_ID__c
        opp = [select id,Owner_ID__c,ownerId from Opportunity where id =: opp.id];
		system.assertEquals(opp.Owner_ID__c,opp.OwnerId  );
        
        //Create a new user and set him as Owner of the oppty
        User usr = createUser('System Administrator');
        opp.OwnerId = usr.Id;
        update opp;
        
        //Confirm after update that the new OwnerId was copied to Owner_ID__c
        opp = [select id,Owner_ID__c,ownerId from Opportunity where id =: opp.id];
		system.assertEquals(opp.Owner_ID__c,usr.Id);

    } //end testBeforeInsertUpdate()
    
    
    //test Method called for after Update
    //Added by Dipika Gupta (T-132472)
    private static testMethod void testAfterUpdate(){
    	
    	// Create a new oppty (will be LTO rectype)
        Opportunity opp = createTestData();
        
        // count the Chatter Posts before the update
        Integer sizeBefore = [Select Id From FeedItem Where ParentId=:UserInfo.getUserId()].size();
        
        // Update the oppty stage to Closed-Won (will fire a chatter post)
        opp.StageName = 'Closed Won';
        opp.EndDate__c = system.today().addDays(1);        
        update opp;
        
        // Confirm that a Chatter Post was added
		system.assertEquals([Select Id From FeedItem Where ParentId=:UserInfo.getUserId()].size(), sizeBefore + 1 );
		
    } //end testAfterUpdate()
    
} //end class TestOpportunityTriggerHandler