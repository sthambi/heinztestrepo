public with sharing class StageHistoryTriggerHandler {
  
  //Method called on after update
  public static void beforeInsert(List< Stage_History__c > liststagehistory){
    for(Stage_History__c s: liststagehistory){
        s.OwnerId = s.Owner_ID__c;

    }
  }
    
}