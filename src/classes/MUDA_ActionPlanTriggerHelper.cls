/**
 *    @author Nikhil More
 *    @date   26/05/2015
      @description    US-0104, US-0107: Track Field History for Action Plans. If the strategy or status is changed, capture this for history report.
      Modification Log: 
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Nikhil More                  26/05/2015            Original Version
 */

Public Class MUDA_ActionPlanTriggerHelper
{

   /*
   *  @author  Nikhil More 
   *  @description US-0104, US-0107: Track Field History for Action Plans. If the strategy or status is changed, capture this for history report.
   *  @param List<Action_Plan__c> lstActionPlanNew, Map<Id, Action_Plan__c> mapActionPlanOld
   *  @return  None.
   */

    public static void handleBeforeUpdateMethod(List<Action_Plan__c> lstActionPlanNew, Map<Id, Action_Plan__c> mapActionPlanOld)
    {
         for(Action_Plan__c apn : lstActionPlanNew)
         {
             if( apn.Strategy__c == 'Re-Deploy' && apn.Status__c == 'Unable to re-deploy' )
             {
                  if(apn.HNZ_Comments__c == mapActionPlanOld.get(apn.Id).HNZ_Comments__c)
                  {
                      apn.HNZ_Comments__c.addError('Please provide appropriate comments!');
                  }
             }
          }
    }

   /*
   *  @author  Nikhil More 
   *  @description US-0104, US-0107: Track Field History for Action Plans. If the strategy or status is changed, capture this for history report.
                                   : Also Add Error if Comments were not changed if inventory could not be redeployed
   *  @param List<Action_Plan__c> lstActionPlanNew, Map<Id, Action_Plan__c> mapActionPlanOld
   *  @return  None.
   */

    public static void handleAfterUpdateMethod(List<Action_Plan__c> lstActionPlanNew, Map<Id, Action_Plan__c> mapActionPlanOld)
    {
         List<Action_Plan_History__c> lstActionHistory = new List<Action_Plan_History__c>();
         for(Action_Plan__c apn : lstActionPlanNew)
          {
              /*
              if(apn.Status__c != mapActionPlanOld.get(apn.Id).Status__c)
              {
                  Action_Plan_History__c aph = new Action_Plan_History__c();
                  
                  aph.Modified_Date__c = apn.LastModifiedDate ;
                  
                  aph.ChangedBy__c = apn.LastModifiedById;
                  
                  aph.Action__c = 'Changed Status from ' + mapActionPlanOld.get(apn.Id).Status__c + ' to ' + apn.Status__c + '.';
                  
                  aph.Action_Plan__c = apn.Id;
                  
                  aph.Field_Name__c = 'Status';
                  
                  aph.Old_Value__c = mapActionPlanOld.get(apn.Id).Status__c ;
                  
                  aph.New_Value__c = apn.Status__c ;
                                    
                  lstActionHistory.add(aph);
              }
              
              if(apn.Strategy__c != mapActionPlanOld.get(apn.Id).Strategy__c)
              {
                  Action_Plan_History__c aph = new Action_Plan_History__c();
                  
                  aph.Modified_Date__c = apn.LastModifiedDate;
                  
                  aph.ChangedBy__c = apn.LastModifiedById;
                  
                  aph.Action__c = 'Changed Strategy from ' + mapActionPlanOld.get(apn.Id).Strategy__c + ' to ' + apn.Strategy__c + '.';
                  
                  aph.Action_Plan__c = apn.Id;
                  
                  aph.Field_Name__c = 'Strategy';
                  
                  aph.Old_Value__c = mapActionPlanOld.get(apn.Id).Strategy__c;
                  
                  aph.New_Value__c = apn.Strategy__c;
                  
                  lstActionHistory.add(aph);
              }
              
              */
              
              if( apn.Strategy__c == 'Re-Deploy' && apn.Status__c == 'Unable to re-deploy' )
              {
                  if(apn.HNZ_Comments__c == mapActionPlanOld.get(apn.Id).HNZ_Comments__c)
                  {
                      apn.HNZ_Comments__c.addError('Please provide appropriate comments!');
                  }
              }
          }
          /*
          try
          {
              if(!lstActionHistory.isEmpty())
              {
                  INSERT lstActionHistory;
              }
          }
          catch(Exception e)
          {
              System.Debug('There was an error while insert '+e.getMessage());   
          }
          */
        
    }  
}