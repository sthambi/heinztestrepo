public with sharing class opportunityStageIndicator {

    public Opportunity opp {get;set;}
    public List<wrapperStage> lstStages {get;set;}
    
    
    public opportunityStageIndicator(ApexPages.StandardController controller) {
        this.opp = (Opportunity)controller.getRecord(); //gets existing record or creates new
        lstStages = new List<wrapperStage>();
        
        Schema.DescribeFieldResult f = Schema.sObjectType.Opportunity.fields.StageName;
        // Get the field describe result from the token
        
        // If current stage is lost, only display that value
        if(opp.StageName == 'Closed Lost' || opp.StageName == 'Rejected Opportunity'){
            lstStages.add(new wrapperstage(opp.StageName));
        }else{ 
            f = f.getSObjectField().getDescribe();
            List <Schema.PicklistEntry> picklistValues = f.getPicklistValues();  
            for (Integer x=0;x<picklistValues.size();x++){
                if(picklistValues[x].getLabel().contains('Lost')
                || picklistValues[x].getLabel().contains('Rejected')){continue;}
                lstStages.add(new wrapperstage(picklistValues[x].getLabel()));
            }
            for(wrapperStage ws: lstStages){
                if(opp.StageName == ws.strStageName){
                    ws.bolCompleted = true;
                    break;
                }else{
                    ws.bolCompleted = true;
                }
            }
        }
    }
    

    
    public class wrapperStage {
        public string strStageName {get;set;}
        public boolean bolCompleted {get;set;}
        public List<wrapperTask> lstTasks {get;set;}
        public wrapperStage(string y){
            strStageName = y; 
            lstTasks = new List<wrapperTask>(); 
        }
    }
    public class wrapperTask {
        public string strTaskName {get;set;}
        public string strFieldName {get;set;}
        public boolean bolSelected {get;set;}
        public wrapperTask(string x, string y, boolean z){
            strTaskName = x;
            strFieldName = y;
            bolSelected = z;            
        }
    }
}