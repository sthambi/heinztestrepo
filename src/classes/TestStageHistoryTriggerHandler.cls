/**
* Class Name   : TestStageHistoryTriggerHandler Class
* Created By   : Doug Reed(Appirio)
* Created Date : 4th June 2013
* Purpose      : test class for StagingHistoryTriggerHandler
**/
@isTest
public with sharing class TestStageHistoryTriggerHandler {
   static testMethod void createTestData(){
    Stage_History__c s = new Stage_History__c();
    s.Stage__c = 'Proposal';
    s.Close_Date__c = system.today();
    s.Owner_ID__c = '005i0000000E0AQ';
    insert s;
  } //end createTestData()

    
} //end class TestOpportunityTriggerHandler