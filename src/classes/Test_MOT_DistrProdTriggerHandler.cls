/***************************************************************************************************************************
*   Type        :   Test Class
*   Name        :   Test_MOT_DistrProdTriggerHandler 
*   Create By   :   Dipendu Chanda   - Deloitte USI
*   Description :   This class is the test class for MOT_DistrProdTriggerHandler
*   Modification Log:
*   --------------------------------------------------------------------------------------
*   * Developer                   Date          Description
*   * ------------------------------------------------------------------------------------                 
*   * Dipendu Chanda                9.06.2015      Created 
*****************************************************************************************************************************/
@isTest
private class Test_MOT_DistrProdTriggerHandler {
    static testMethod void updateDistrProdIdTest() {
        //Only Heinz sales user 
        user usrHNZ = TestDataUtility.createUserData();
        system.assert(usrHNZ.id!=null);
        System.runAs(usrHNZ){
            //Distributor accounts inserted
            List<Account> accLst = TestDataUtility.createAccountsData();
                accLst[0].Account_Type__c='Distributor';
                accLst[1].Account_Type__c='Distributor';
                update accLst;
                system.assert(accLst!=null);
            //Product list inserted
            List<Product2> prodLst = TestDataUtility.createMOTProductsData();
                system.assert(prodLst!=null);
            //Distributor product 1 and 2 inserted
            List<Distributor_Products__c> dpList = new List<Distributor_Products__c>();
            Distributor_Products__c dp1 = new Distributor_Products__c(  Account__c=accLst[0].id,
                                                                        Product__c=prodLst[0].id);
                dpList.add(dp1);
            Distributor_Products__c dp2 = new Distributor_Products__c(  Account__c=accLst[1].id,
                                                                        Product__c=prodLst[1].id);
                dpList.add(dp2);
                insert dpList;
                system.assert(dpList!=null);
            //Distributor product 3 insertion failed because of duplicate rule fired before insertion.
            try{
            List<Distributor_Products__c> dpDupList = new List<Distributor_Products__c>();
            Distributor_Products__c dp3 = new Distributor_Products__c(  Account__c=accLst[1].id,
                                                                        Product__c=prodLst[1].id);
                dpDupList.add(dp3);
            Distributor_Products__c dp4 = new Distributor_Products__c(  Account__c=accLst[0].id,
                                                                        Product__c=prodLst[0].id);
                dpDupList.add(dp4);
                //insertion would fail because of duplicate record.
                insert dpDupList;
                }
            catch(DMLException ex){
                //Showing the exception that occured ie."DUPLICATES_DETECTED"
                System.Debug('There was an error ' + ex.getMessage());
                }
        }
    }
}