public with sharing class OpportunityTriggerHandler {
  
  //Method called on before update
  public static void isBeforeUpdate(List<opportunity> newList,Map<id,opportunity> oldMap){
    
	List<RecordType> listOfRecType = new List<RecordType>(
										[Select r.SobjectType, r.DeveloperName, r.Id 
										 From RecordType r
										 where  r.DeveloperName in ('Opportunity','Opportunity_LTO_Layout') ]);
                                         
    map<String,id> mapOfRecType = new map<String,id> ();
    
    for(RecordType rec : listOfRecType){
        mapOfRecType.put(rec.DeveloperName,rec.id);
    }    
                                
    for(Opportunity o : newList){
        o.Owner_ID__c = o.ownerId; //Added by Dipika Gupta (T-132472)
        
        if(o.StageName == 'Closed Won' && oldMap.get(o.id).StageName != o.StageName){
        	o.hasClosed__c = true;
                o.Closed_Won_Date__c = datetime.now();
        }
        
        if(o.LTO_K_12__c != oldMap.get(o.id).LTO_K_12__c){
            if(o.LTO_K_12__c == true){
                if(mapOfRecType.get('Opportunity_LTO_Layout') != null)
                   o.RecordTypeId = mapOfRecType.get('Opportunity_LTO_Layout');
            }
            else{
                if(mapOfRecType.get('Opportunity') != null)
                    o.RecordTypeId = mapOfRecType.get('Opportunity');
            }
         }
     }
  }
  
  //Method called on before insert
  //Added by Dipika Gupta (T-132472)
public static void beforeInsert(List<opportunity> listopportunity){
    List<RecordType> listOfRecType = new List<RecordType>(
                                        [Select r.SobjectType, r.DeveloperName, r.Id 
                                         From RecordType r
                                         where  r.DeveloperName in ('Opportunity','Opportunity_LTO_Layout') ]);
                                         
    map<String,id> mapOfRecType = new map<String,id> ();
    
    for(RecordType rec : listOfRecType){
        mapOfRecType.put(rec.DeveloperName,rec.id);
    }
    for(Opportunity opp: listopportunity){
        opp.Owner_ID__c = opp.ownerId;
        if(opp.LTO_K_12__c == true){
            if(mapOfRecType.get('Opportunity_LTO_Layout') != null)
               opp.RecordTypeId = mapOfRecType.get('Opportunity_LTO_Layout');
        }
    }
}
  
  //Method called on after update
  //Added by Dipika Gupta (T-132472)
  public static void afterUpdate(List<opportunity> listopportunity ,Map<id,opportunity> mapOpportunity){
    for(Opportunity opp : listopportunity){
        if(opp.IsWon == true && mapOpportunity.get(opp.id).IsWon == false){
            String oppURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.id;
            String status = '#Winning - We just won ' + opp.Name +'! ' + opp.Total_Case_Volume__c + ' Cases' ;
            FeedItem post = new FeedItem();
            post.ParentId = UserInfo.getUserId();
            post.Title = opp.Name;
            post.Body = status ;
            post.LinkUrl = oppURL;
            post.Type = 'LinkPost';
            insert post;
        }
    }
  }
    
}