/**
 *    @author Nikhil More
 *    @date   03/06/2015
      @description : To Populate the Demand Plant Code once the Parent Demand Plant is specified for the plant
      Modification Log: 
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Nikhil More                  26/05/2015            Original Version
 */

trigger MUDA_PlantTrigger on Plant__c(Before insert, Before Update)
{
    if(Trigger.isBefore && Trigger.isInsert)
    {
        MUDA_PlantTriggerHelper.handleBeforeInsertMethod(Trigger.new); 
    }
    if(Trigger.isBefore && Trigger.isUpdate)
    {
        MUDA_PlantTriggerHelper.handleBeforeUpdateMethod(Trigger.new);
    }
}