/**
 *    @author Nikhil More
 *    @date   26/05/2015
      @description US-0104, US-0107: Track Field History for Action Plans. If the strategy or status is changed, capture this for history report.
      Modification Log: 
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Nikhil More                  26/05/2015            Original Version
 */

trigger MUDA_ActionPlanTrigger on Action_Plan__c (after update)
{
    if(Trigger.isBefore && Trigger.isUpdate)
    {
        MUDA_ActionPlanTriggerHelper.handleBeforeUpdateMethod(Trigger.new, Trigger.oldMap);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate)
    {
        MUDA_ActionPlanTriggerHelper.handleAfterUpdateMethod(Trigger.new, Trigger.oldMap);
    }
}