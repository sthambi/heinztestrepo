/*
    * Developed by : Ankita
    *
    * Functionality : Automatically populate the pricebook to MOT-US for Heinz Sales Profile users
    *
    Date:       Developer:      Modification Description:
    5/26/2015    (Ankita)        created.
                                 
    */
    trigger OrderTrigger on Order (before insert) {
        MOT_OrderTriggerHandler.updatePriceBook(Trigger.New);           
    }