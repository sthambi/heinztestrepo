/*
    * Developed by : Ankita
    *
    * Functionality : Trigger to Handle OrderProduct.
    *
    Date:       Developer:      Modification Description:
    06/02/2015    (Ankita)        created. Functionality:uniqueProductNull
    18/06/2015    (Dipendu)       modified. Functionality:UniqueProduct
                                 
    */
    trigger OrderProductTrigger on OrderItem (before insert) {
       //MOT:Automatically nulls the uniqueproduct id field when order is cloned with products
       MOT_OrderProductTriggerHandler.uniqueProductNull(Trigger.New);
       //MOT:Automatically Checks duplicate product existed already or not
       MOT_OrderProductTriggerHandler.UniqueProduct(Trigger.New);
       
  }