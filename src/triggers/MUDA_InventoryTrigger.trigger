/**
 *    @author Nikhil More
 *    @date   26/05/2015
      @description: This trigger handles the inventory reload to reset the processed flag when inventory records are updated.
      Modification Log: NA
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Nikhil More                  30/05/2015            Original Version
**/
trigger MUDA_InventoryTrigger on Material_Plant_Batch__c(before insert, before update)
{
    /*
    if(Trigger.isBefore && Trigger.isInsert)
    {
        for(Material_Plant_Batch__c mpb : Trigger.newMap)
        {
                mpb.Demand_Plant__c = mpb.Plant__r.Demand_Plant__c;
                mpb.Demand_Plant_Code__c = mpb.Plant__r.Demand_Plant_Code__c;
        }
    }
    */
    if(Trigger.IsBefore && Trigger.isInsert)
    {
        MUDA_InventoryTriggerHandler.MUDA_Assign_Planner(Trigger.New);
    }
    
    /*
    if(Trigger.isBefore && Trigger.isUpdate)
    {
        Boolean isInventoryReload = MUDA_Data_Reload__c.getInstance('Inventory Update').IsDataReload__c;
        
        if(isInventoryReload)
        {
            for(Material_Plant_Batch__c mpb : Trigger.new)
            {
                System.Debug('FLAG '+ mpb.Demand_Assigned__c+' isInventoryReload '+isInventoryReload);
                
                mpb.Demand_Assigned__c = false;
            }
        }
    }
    */
}