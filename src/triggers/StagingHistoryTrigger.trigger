trigger StagingHistoryTrigger on Stage_History__c (before insert,before update) {

  if(Trigger.isBefore && Trigger.isInsert){
         StageHistoryTriggerHandler.beforeInsert(Trigger.new);
   }
}