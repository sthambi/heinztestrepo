trigger MUDA_InventoryOwnerTrigger on Inventory_Owner__c (Before Insert) {
       if(Trigger.isInsert && Trigger.isBefore){
               MUDA_InventoryOwnerTriggerHandler.stampPlantMaterialCode(Trigger.New);
       }
}