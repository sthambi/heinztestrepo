/**
 *    @author Nikhil More
 *    @date   26/05/2015
      @description: This trigger handles the forecast assignment to inventory when demand records are updated.
      Modification Log: NA
      ------------------------------------------------------------------------------------        
      Developer                       Date                Description        
      ------------------------------------------------------------------------------------        
      Nikhil More                  30/05/2015            Original Version
**/
trigger MUDA_ForecastTrigger on Demand_Forecast__c (after update)
{
    if(Trigger.isAfter && Trigger.isUpdate)
    {
        Boolean isDataReload = MUDA_Data_Reload__c.getInstance('Forecast Update').IsDataReload__c;
        
        if(isDataReload)
        {
            MUDA_ForecastTriggerHelper.ForecastAssignmentMethod(Trigger.new);
        }
    }
}