trigger OpportunityTrigger on Opportunity (after update,before update,before insert) {
   
   //Updated by Dipika Gupta (T-132472)
   if(Trigger.isAfter && Trigger.isUpdate){
     OpportunityTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
   }
   //updated by kirti agarwal
   if(Trigger.isBefore){
       if(Trigger.isUpdate){
         OpportunityTriggerHandler.isBeforeUpdate(Trigger.new, Trigger.oldMap);
         
       }
   }
   
   //Added by Dipika Gupta (T-132472)
   if(Trigger.isBefore && Trigger.isInsert){
         OpportunityTriggerHandler.beforeInsert(Trigger.new);
   }
 
}