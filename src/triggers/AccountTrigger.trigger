/*
    * Developed by : Dipendu
    *
    * Functionality : Automatically enable all MOT products for a new MOT distributor
    *
    Date:       Developer:      Modification Description:
    6/4/2015    (Dipendu)        created.
                                 
    */
    trigger AccountTrigger on Account (after update,after insert) {
        if(Trigger.isAfter && (Trigger.IsUpdate||Trigger.IsInsert)){
                //Created for MOT: Automatically enable all MOT products for a new MOT distributor.
                MOT_AccountTriggerHandler.updateMotProdOnDistributor(Trigger.New); 
        }         
    }