/*
    * Developed by : Dipendu
    *
    * Functionality : Automatically populate the Product ID text field with the product value before insert.
    *
    Date:       Developer:      Modification Description:
    6/09/2015    (Dipendu)        created.
                                 
    */
trigger DistributorProductTrigger on Distributor_Products__c (before insert, before update) {
    if(Trigger.isBefore){
        if(Trigger.IsUpdate||Trigger.IsInsert){
          MOT_DistrProdTriggerHandler.FillProdId(Trigger.new);
         }
    }

}