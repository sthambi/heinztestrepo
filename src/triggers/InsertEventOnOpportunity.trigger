/*-------------------------------------------------------------------------------------
* InsertEventOnOpportunity trigger
*
* Trigger for Opportunity. Inserts related event to show up on the calender
*
* (c) Appirio 2014
*
* Created By   : Nikita Jain 05/26/2014
*
* -------------------------------------------------------------------------------------
*/
trigger InsertEventOnOpportunity on Opportunity (after insert, after update) {
    list<Task> eventsToBeInserted = new list<Task>();
    if(trigger.isInsert) {
        for(Opportunity opportunity: trigger.new) {
            Task event = new Task();
            event.OwnerId = opportunity.Owner_ID__c;
            event.Subject = opportunity.Name + ' - ' + 'Closing Task';
            event.Type = 'Other';
            event.WhatId = opportunity.Id;
            event.Status = 'Not Started';
            event.Priority = 'Normal';
            event.ActivityDate = opportunity.CloseDate;
            eventsToBeInserted.add(event);
        }
        if(eventsToBeInserted.size() > 0) {
            insert eventsToBeInserted;
        }
    } else {
        map<Id, list<Task>> mapOppIdEvent = new map<Id, list<Task>>();
        list<Task> relatedEventList = new list<Task>();
        for(Task evnt: [Select id,whatId from Task where whatId in : trigger.new]) {
            if(mapOppIdEvent.containsKey(evnt.whatId)) {
                mapOppIdEvent.get(evnt.whatId).add(evnt);
            } else {
                list<Task> eventList = new list<Task>();
                eventList.add(evnt);
                mapOppIdEvent.put(evnt.whatId, eventList);
            }
        }
        for(Opportunity opportunity: trigger.new) {
            if(opportunity.CloseDate != trigger.oldMap.get(opportunity.Id).CloseDate && mapOppIdEvent.containsKey(opportunity.Id)) {
                for(Task event: mapOppIdEvent.get(opportunity.Id)) {
                    event.ActivityDate = opportunity.CloseDate;
                    relatedEventList.add(event);
                }
            }
        }
        if(relatedEventList.size() > 0) {
            update relatedEventList;
        }
    }
}